package main

import "sort"

func minimumSum(num int) int {
	digits := getDigitsSorted(num)

	return digits[0]*10 + digits[2] + digits[1]*10 + digits[3]
}

func getDigitsSorted(num int) []int {
	digits := make([]int, 4)

	digits[0] = num / 1000
	num %= 1000
	digits[1] = num / 100
	num %= 100
	digits[2] = num / 10
	digits[3] = num % 10

	sort.Ints(digits)

	return digits
}
