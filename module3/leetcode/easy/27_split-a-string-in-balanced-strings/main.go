package main

func balancedStringSplit(s string) int {
	count := 0
	symbolsCounter := make(map[rune]int)
	newCount := true

	for _, l := range s {
		symbolsCounter[l]++
		if !newCount && symbolsCounter['R'] == symbolsCounter['L'] {
			count++
			symbolsCounter['R'] = 0
			symbolsCounter['L'] = 0
			newCount = true
			continue
		}
		newCount = false
	}

	return count
}
