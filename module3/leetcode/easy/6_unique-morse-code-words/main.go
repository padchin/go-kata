package main

import "fmt"

func uniqueMorseRepresentations(words []string) int {
	controlMap := make(map[string]struct{})

	morse := buildMap()

	for _, word := range words {
		var image []byte
		for _, letter := range []byte(word) {
			image = append(image, []byte(morse[letter])...)
		}
		controlMap[string(image)] = struct{}{}
	}

	return len(controlMap)
}

func buildMap() map[byte]string {
	m := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	_map := make(map[byte]string, len(m))
	for i, v := range m {
		_map[byte('a'+i)] = v
	}
	return _map
}

func main() {
	fmt.Println(uniqueMorseRepresentations([]string{"gin", "zen", "gig", "msg"}))
}
