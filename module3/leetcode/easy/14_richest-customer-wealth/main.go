package main

import "sort"

func maximumWealth(accounts [][]int) int {
	var sums []int

	for _, account := range accounts {
		sum := 0
		for _, v := range account {
			sum += v
		}
		sums = append(sums, sum)
	}

	sort.Ints(sums)

	return sums[len(sums)-1]
}
