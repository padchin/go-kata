package main

import "math"

func subtractProductAndSum(n int) int {
	return getDigitsProduct(n) - getDigitsSum(n)
}

func getDigitsSum(num int) int {
	sum := 0
	sum += num / 10000
	num %= 10000
	sum += num / 1000
	num %= 1000
	sum += num / 100
	num %= 100
	sum += num / 10
	sum += num % 10

	return sum
}

func getDigitsProduct(num int) int {
	numDigits := int(math.Log10(float64(num))) + 1

	product := 1
	for i := numDigits; i >= 1; i-- {
		product *= int(float64(num) / math.Pow10(i-1))
		num %= int(math.Pow10(i - 1))
	}

	return product
}
