package main

import (
	"reflect"
	"testing"
)

func TestConstructor(t *testing.T) {
	type args struct {
		big    int
		medium int
		small  int
	}
	tests := []struct {
		name string
		args args
		want ParkingSystem
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Constructor(tt.args.big, tt.args.medium, tt.args.small); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Constructor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParkingSystem_AddCar(t *testing.T) {
	type fields struct {
		big    int
		medium int
		small  int
	}
	type args struct {
		carType int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			this := &ParkingSystem{
				big:    tt.fields.big,
				medium: tt.fields.medium,
				small:  tt.fields.small,
			}
			if got := this.AddCar(tt.args.carType); got != tt.want {
				t.Errorf("AddCar() = %v, want %v", got, tt.want)
			}
		})
	}
}
