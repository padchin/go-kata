package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
	max := getMaxCandies(candies)

	res := make([]bool, len(candies))
	for i, v := range candies {
		if v+extraCandies >= max {
			res[i] = true
		}
	}

	return res
}

func getMaxCandies(candies []int) int {
	max := 0
	for _, v := range candies {
		if v > max {
			max = v
		}
	}

	return max
}
