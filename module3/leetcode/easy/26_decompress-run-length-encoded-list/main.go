package main

func decompressRLElist(nums []int) []int {
	var res []int
	for i := 0; i < len(nums)/2; i++ {
		res = append(res, fill(nums[i*2+1], nums[i*2])...)
	}
	return res
}

func fill(value, count int) []int {
	var res []int
	for i := 0; i < count; i++ {
		res = append(res, value)
	}
	return res
}
