package main

func finalValueAfterOperations(operations []string) int {
	minus := byte('-')
	plus := byte('+')
	sum := 0

	for _, op := range operations {
		switch op[1] {
		case minus:
			sum--
		case plus:
			sum++
		}
	}

	return sum
}
