package main

func createTargetArray(nums []int, index []int) []int {
	var res []int
	for i := range nums {
		res = append(res, 0)
		copy(res[index[i]+1:], res[index[i]:])
		res[index[i]] = nums[i]
	}
	return res
}
