package main

// Since that encoded[i] = arr[i] XOR arr[i+1], then arr[i+1] = encoded[i] XOR arr[i].
// Iterate on i from beginning to end, and set arr[i+1] = encoded[i] XOR arr[i].
func decode(encoded []int, first int) []int {
	res := []int{first}

	for _, x := range encoded {
		res = append(res, x^res[len(res)-1])
	}

	return res
}
