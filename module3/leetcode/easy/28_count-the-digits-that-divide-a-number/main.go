package main

import "math"

func countDigits(num int) int {
	counter := 0

	for _, v := range getDigits(num) {
		if num%v == 0 {
			counter++
		}
	}

	return counter
}

func getDigits(num int) []int {
	digitsCount := int(math.Log10(float64(num))) + 1

	var res []int

	for i := digitsCount; i >= 1; i-- {
		res = append(res, num/int(math.Pow10(i-1)))
		num %= int(math.Pow10(i - 1))
	}

	return res
}
