package main

// Input: arr = [2,3,4,7,11], k = 5
// Output: 9
// Explanation: The missing positive integers are [1,5,6,8,9,10,12,13,...]. The 5th missing positive integer is 9.

func findKthPositive(arr []int, k int) int {
	pArr := 0
	missed := make([]int, 0)
	for x := 1; ; x++ {
		if x != arr[pArr] {
			missed = append(missed, x)
		} else {
			pArr++
			if pArr == len(arr) {
				break
			}
		}
	}
	if k <= len(missed) {
		return missed[k-1]
	}

	lenMissed := len(missed)
	if lenMissed > 0 && arr[len(arr)-1] > missed[lenMissed-1] {
		return k - lenMissed + arr[len(arr)-1]
	}

	if lenMissed > 0 {
		return missed[lenMissed-1] + k - lenMissed
	}

	return arr[len(arr)-1] + k
}
