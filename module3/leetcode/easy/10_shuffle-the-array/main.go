package main

func shuffle(nums []int, n int) []int {
	res := make([]int, 2*n)

	for i := range nums {
		res[i*2], res[i*2+1] = nums[i], nums[i+n]
		if i == n-1 {
			break
		}
	}

	return res
}
