package main

func numJewelsInStones(jewels string, stones string) int {
	j := make(map[byte]struct{})

	for _, jewel := range jewels {
		j[byte(jewel)] = struct{}{}
	}

	count := 0
	for _, stone := range stones {
		if _, ok := j[byte(stone)]; ok {
			count++
		}
	}

	return count
}
