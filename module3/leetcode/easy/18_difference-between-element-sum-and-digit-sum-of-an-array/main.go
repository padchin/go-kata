package main

func differenceOfSum(nums []int) int {
	digitsSum := 0
	numbersSum := 0
	for _, v := range nums {
		digitsSum += getDigitsSum(v)
		numbersSum += v
	}
	return numbersSum - digitsSum
}

func getDigitsSum(num int) int {
	sum := 0
	sum += num / 1000
	num %= 1000
	sum += num / 100
	num %= 100
	sum += num / 10
	sum += num % 10

	return sum
}
