package main

import "sort"

func sortPeople(names []string, heights []int) []string {
	var res []string
	var persons []Person

	for i := range names {
		persons = append(persons, Person{
			name:   names[i],
			height: heights[i],
		})
	}

	sort.Slice(persons, func(i, j int) bool {
		return persons[i].height > persons[j].height
	})

	for i := range persons {
		res = append(res, persons[i].name)
	}

	return res
}

type Person struct {
	name   string
	height int
}
