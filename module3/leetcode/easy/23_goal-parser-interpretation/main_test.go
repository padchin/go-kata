package main

import "testing"

func Test_interpret(t *testing.T) {
	type args struct {
		command string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "1",
			args: args{
				command: "G()(al)",
			},
			want: "Goal",
		},
		{
			name: "2",
			args: args{
				command: "(al)G(al)()()G",
			},
			want: "alGalooG",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := interpret(tt.args.command); got != tt.want {
				t.Errorf("interpret() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_interpret2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		interpret2("GGG()()()()()()(al)GGGG(al)")
	}
}

func Benchmark_interpret(b *testing.B) {
	for i := 0; i < b.N; i++ {
		interpret("GGG()()()()()()(al)GGGG(al)")
	}
}
