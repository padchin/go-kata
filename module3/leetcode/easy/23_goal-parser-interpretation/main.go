package main

import (
	"strings"
)

func interpret(command string) string {
	byteCommand := []byte(command)
	var res []byte
	p := 0
	for p < len(byteCommand) {
		if byteCommand[p] == 'G' {
			res = append(res, 'G')
			p++
			continue
		}
		if p < len(byteCommand)-1 && byteCommand[p+1] == ')' {
			res = append(res, 'o')
			p++
			p++
			continue
		}
		res = append(res, 'a', 'l')
		p += 4
	}

	return string(res)
}

func interpret2(command string) string {
	return strings.ReplaceAll(strings.ReplaceAll(command, "()", "o"), "(al)", "al")
}
