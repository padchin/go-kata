package main

func runningSum(nums []int) []int {
	res := make([]int, len(nums))

	sum := 0

	for i, v := range nums {
		sum += v
		res[i] = sum
	}
	return res
}
