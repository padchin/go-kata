package main

func numIdenticalPairs(nums []int) int {
	// A pair (i, j) is called good if nums[i] == nums[j] and i < j
	count := 0
	for i := range nums {
		for j := range nums {
			if nums[i] == nums[j] && i < j {
				count++
			}
		}
	}

	return count
}
