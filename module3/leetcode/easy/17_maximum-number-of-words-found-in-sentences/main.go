package main

func mostWordsFound(sentences []string) int {
	max := 0
	space := byte(' ')

	for _, sentence := range sentences {
		spaceCount := 0
		for _, symbol := range sentence {
			if byte(symbol) == space {
				spaceCount++
			}
		}

		if spaceCount+1 > max {
			max = spaceCount + 1
		}
	}

	return max
}
