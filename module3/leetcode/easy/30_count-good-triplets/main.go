package main

func countGoodTriplets(arr []int, a int, b int, c int) int {
	counter := 0
	size := len(arr)
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			for k := 0; k < size; k++ {
				if i < j && j < k {
					if abs(arr[i]-arr[j]) <= a {
						if abs(arr[j]-arr[k]) <= b {
							if abs(arr[i]-arr[k]) <= c {
								counter++
							}
						}
					}
				}
			}
		}
	}

	return counter
}

func abs(n int) int {
	if n < 0 {
		return -n
	}
	return n
}
