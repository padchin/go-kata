package main

func smallerNumbersThanCurrent(nums []int) []int {
	checked := make(map[int]int)
	var res []int

	for i := 0; i < len(nums); i++ {
		if v, ok := checked[i]; ok {
			res = append(res, v)
			continue
		}

		for j := 0; j < len(nums); j++ {
			if j != i && nums[j] < nums[i] {
				checked[i]++
			}
		}

		res = append(res, checked[i])
	}

	return res
}
