package main

func maxSum(grid [][]int) int {
	max := 0
	sizeY := len(grid)
	sizeX := len(grid[0])
	for y := 0; y <= sizeY-3; y++ {
		for x := 0; x <= sizeX-3; x++ {
			sum := grid[y][x]
			sum += grid[y][x+1]
			sum += grid[y][x+2]
			sum += grid[y+1][x+1]
			sum += grid[y+2][x]
			sum += grid[y+2][x+1]
			sum += grid[y+2][x+2]

			if sum > max {
				max = sum
			}
		}
	}

	return max
}
