package main

import "testing"

func Test_maxSum(t *testing.T) {
	type args struct {
		grid [][]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "1",
			args: args{
				grid: [][]int{{6, 2, 1, 3}, {4, 2, 1, 5}, {9, 2, 8, 7}, {4, 1, 2, 9}},
			},
			want: 30,
		},
		{
			name: "2",
			args: args{
				grid: [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}},
			},
			want: 35,
		},
		{
			name: "3",
			args: args{
				grid: [][]int{{520626, 685427, 788912, 800638, 717251, 683428}, {23602, 608915, 697585, 957500, 154778, 209236}, {287585, 588801, 818234, 73530, 939116, 252369}},
			},
			want: 5095181,
		},
		{
			name: "4",
			args: args{
				grid: [][]int{{1, 2, 3, 4, 5, 6}, {7, 8, 9, 9, 8, 7}, {6, 5, 4, 3, 2, 1}},
			},
			want: 30,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := maxSum(tt.args.grid); got != tt.want {
				t.Errorf("maxSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
