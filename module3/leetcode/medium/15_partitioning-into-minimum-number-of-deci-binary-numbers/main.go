package main

func minPartitions(n string) int {
	b := []byte(n)

	var maxDigit byte

	for _, v := range b {
		if v > maxDigit {
			maxDigit = v
		}
	}

	return int(maxDigit - '0')
}
