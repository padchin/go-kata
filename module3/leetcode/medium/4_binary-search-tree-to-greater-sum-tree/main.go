package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func bstToGst(root *TreeNode) *TreeNode {
	var sum int
	var traverse func(root *TreeNode)
	traverse = func(root *TreeNode) {
		if root == nil {
			return
		}

		traverse(root.Right)

		sum += root.Val
		root.Val = sum

		traverse(root.Left)
	}

	traverse(root)

	return root
}

/*
The following is an alternative, where the tree is being traversed from left to right.
Also works correctly on LeetCode.
*/

//func bstToGstAlt(root *TreeNode) *TreeNode {
//	sumArray := buildSumArray(getSortedNodes(root))
//
//	rebuildTree(root, sumArray)
//	return root
//}

func fillBT(nums []int) *TreeNode {
	n := len(nums)
	if n == 0 {
		return nil
	}

	root := &TreeNode{Val: nums[0]}
	queue := []*TreeNode{root}

	for i := 1; i < n; i += 2 {
		parent := queue[0]
		queue = queue[1:]

		if nums[i] != -1 {
			left := &TreeNode{Val: nums[i]}
			parent.Left = left
			queue = append(queue, left)
		}

		if i+1 < n && nums[i+1] != -1 {
			right := &TreeNode{Val: nums[i+1]}
			parent.Right = right
			queue = append(queue, right)
		}
	}

	return root
}

func getSortedNodes(root *TreeNode) []Node {
	var res []Node
	if root == nil {
		return res
	}

	res = append(res, getSortedNodes(root.Left)...)
	res = append(res, Node{
		node:  root,
		value: root.Val,
	})
	res = append(res, getSortedNodes(root.Right)...)

	return res
}

type Node struct {
	node  *TreeNode
	value int
}

func rebuildTree(root *TreeNode, sumArray []Node) {
	if root == nil {
		return
	}

	rebuildTree(root.Left, sumArray)

	for _, x := range sumArray {
		if root == x.node {
			root.Val += x.value
			break
		}
	}

	rebuildTree(root.Right, sumArray)
}

func buildSumArray(sortedNodes []Node) []Node {
	size := len(sortedNodes)
	res := make([]Node, size)

	for i := 0; i < size-1; i++ {
		sum := 0
		for j := size - 1; j > i; j-- {
			sum += sortedNodes[j].value
		}
		res[i].value = sum
		res[i].node = sortedNodes[i].node
	}

	return res
}

func main() {
	nums := []int{4, 1, 6, 0, 2, 5, 7, -1, -1, -1, 3, -1, -1, -1, 8}

	root := fillBT(nums)

	sumArray := buildSumArray(getSortedNodes(root))

	rebuildTree(root, sumArray)
	fmt.Println()
}
