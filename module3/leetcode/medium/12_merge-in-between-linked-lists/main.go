package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	node2 := getNodeByIndex(list1, a-1)
	node5 := getNodeByIndex(list1, b+1)
	node2.Next = list2
	node10002 := getLastNode(list2)
	node10002.Next = node5

	return list1
}

func getNodeByIndex(list *ListNode, index int) *ListNode {
	if index == 0 {
		return list
	}

	for i := 0; i < index; i++ {
		list = list.Next
	}

	return list
}

func getLastNode(list *ListNode) *ListNode {
	for {
		if list.Next == nil {
			return list
		}
		list = list.Next
	}
}
