package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func pairSum(head *ListNode) int {
	var list []int

	for head.Next != nil {
		list = append(list, head.Val)
		head = head.Next
	}
	list = append(list, head.Val)

	var maxSum int
	for i, j := 0, len(list)-1; i < j; i, j = i+1, j-1 {
		sum := list[j] + list[i]
		if sum > maxSum {
			maxSum = sum
		}
	}

	return maxSum
}

func main() {
	head := buildList([]int{4, 2, 2, 3})
	fmt.Println(pairSum(head))
}

func buildList(nums []int) *ListNode {
	head := &ListNode{
		Val:  nums[0],
		Next: nil,
	}

	node := head
	for x := 1; x < len(nums); x++ {
		newNode := &ListNode{
			Val:  nums[x],
			Next: nil,
		}
		node.Next = newNode
		node = newNode
	}
	return head
}
