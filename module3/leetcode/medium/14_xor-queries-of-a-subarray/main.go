package main

func xorQueries(arr []int, queries [][]int) []int {
	res := make([]int, len(queries))

	for i, v := range queries {
		xor := 0
		idxLeft, idxRight := v[0], v[1]

		for idxLeft <= idxRight {
			xor ^= arr[idxLeft]
			idxLeft++
		}

		res[i] = xor
	}

	return res
}
