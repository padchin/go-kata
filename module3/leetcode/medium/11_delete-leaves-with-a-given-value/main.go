package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	var deleted bool
	root, deleted = iter(root, target)

	for deleted {
		root, deleted = iter(root, target)
	}

	return root
}

func iter(root *TreeNode, target int) (*TreeNode, bool) {
	if root == nil {
		return root, false
	}

	if root.Left == nil && root.Right == nil {
		if root.Val == target {
			return nil, true
		}

		return root, false
	}

	deletedLeft := false
	deletedRight := false

	if root != nil {
		root.Left, deletedLeft = iter(root.Left, target)
	}
	if root != nil {
		root.Right, deletedRight = iter(root.Right, target)
	}

	return root, deletedLeft || deletedRight
}

func fillBT(nums []int) *TreeNode {
	n := len(nums)
	if n == 0 {
		return nil
	}

	root := &TreeNode{Val: nums[0]}
	queue := []*TreeNode{root}

	for i := 1; i < n; i += 2 {
		parent := queue[0]
		queue = queue[1:]

		if nums[i] != 0 {
			left := &TreeNode{Val: nums[i]}
			parent.Left = left
			queue = append(queue, left)
		}

		if i+1 < n && nums[i+1] != 0 {
			right := &TreeNode{Val: nums[i+1]}
			parent.Right = right
			queue = append(queue, right)
		}
	}

	return root
}

func main() {
	root := fillBT([]int{1, 2, 3, 2, 0, 2, 4})

	newRoot := removeLeafNodes(root, 2)

	_ = newRoot
}
