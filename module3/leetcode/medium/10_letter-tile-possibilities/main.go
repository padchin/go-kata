package main

import "fmt"

//func numTilePossibilities(tiles string) int {
//
//}
//
//func findCombinations(tiles string, sol []string) []string {
//	if l == r {
//		return []string{string(tiles[r])}
//	}
//
//}

func numTilePossibilities(tiles string) int {
	tilesMap := make(map[string]int)
	for _, symbol := range tiles {
		tilesMap[string(symbol)]++
	}

	return countCombinations(&tilesMap)
}

func countCombinations(tilesMap *map[string]int) int {
	sum := 0
	for symbol, freq := range *tilesMap {
		if freq == 0 {
			continue
		}

		sum++
		(*tilesMap)[symbol]--
		sum += countCombinations(tilesMap)
		(*tilesMap)[symbol]++
	}

	return sum
}

func main() {
	fmt.Println(numTilePossibilities("AAABBCD"))
}
