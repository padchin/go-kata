package main

import "sort"

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	res := make([]bool, len(l))
check:
	for x := range l {
		test := append([]int{}, nums[l[x]:r[x]+1]...)
		sort.Ints(test)
		delta := test[1] - test[0]

		for z := 2; z < len(test); z++ {
			if test[z]-test[z-1] != delta {
				res[x] = false
				continue check
			}
		}

		res[x] = true
	}

	return res
}
