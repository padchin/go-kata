package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	a := make(map[int]struct{})
	var res []int
	b := make(map[int]struct{})

	for _, v := range edges {
		a[v[1]] = struct{}{}
	}

	for _, v := range edges {
		if _, ok := a[v[0]]; !ok {
			b[v[0]] = struct{}{}
		}
	}

	for k := range b {
		res = append(res, k)
	}

	return res
}
