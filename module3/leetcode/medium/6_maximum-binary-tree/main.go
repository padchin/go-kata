package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	pivot := getMaxElementIndex(nums)

	node := &TreeNode{
		Val:   nums[pivot],
		Left:  nil,
		Right: nil,
	}

	left := nums[:pivot]
	right := nums[pivot+1:]

	if len(left) > 0 {
		node.Left = constructMaximumBinaryTree(left)
	}

	if len(right) > 0 {
		node.Right = constructMaximumBinaryTree(right)
	}

	return node
}

func getMaxElementIndex(nums []int) int {
	index, max := 0, 0
	for i, v := range nums {
		if v > max {
			index = i
			max = v
		}
	}

	return index
}
