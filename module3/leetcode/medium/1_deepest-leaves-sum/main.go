package main

import (
	"fmt"
	"sort"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	leafs := getLeafs(root)

	sort.Slice(leafs, func(i, j int) bool {
		return leafs[i].depth > leafs[j].depth
	})

	maxDepth := leafs[0].depth
	depth := maxDepth
	i := 0

	sum := 0

	for depth == maxDepth {
		sum += leafs[i].value
		i++
		if i <= len(leafs) {
			depth = leafs[i].depth
		} else {
			break
		}
	}

	return sum
}

type Leaf struct {
	depth int
	value int
}

func getLeafs(root *TreeNode) []Leaf {
	var result []Leaf

	if root == nil {
		return result
	}

	if root.Left == nil && root.Right == nil {
		result = append(result, Leaf{
			depth: 0,
			value: root.Val,
		})
		return result
	}

	leftLeafs := getLeafs(root.Left)
	rightLeafs := getLeafs(root.Right)

	for _, v := range leftLeafs {
		result = append(result, Leaf{
			depth: v.depth + 1,
			value: v.value,
		})
	}

	for _, v := range rightLeafs {
		result = append(result, Leaf{
			depth: v.depth + 1,
			value: v.value,
		})
	}

	return result
}

func fillBT(nums []int) *TreeNode {
	n := len(nums)
	if n == 0 {
		return nil
	}

	root := &TreeNode{Val: nums[0]}
	queue := []*TreeNode{root}

	for i := 1; i < n; i += 2 {
		parent := queue[0]
		queue = queue[1:]

		if nums[i] != 0 {
			left := &TreeNode{Val: nums[i]}
			parent.Left = left
			queue = append(queue, left)
		}

		if i+1 < n && nums[i+1] != 0 {
			right := &TreeNode{Val: nums[i+1]}
			parent.Right = right
			queue = append(queue, right)
		}
	}

	return root
}

func main() {
	nums := []int{6, 7, 8, 2, 7, 1, 3, 9, 0, 1, 4, 0, 0, 0, 5}

	root := fillBT(nums)

	fmt.Println(deepestLeavesSum(root))
}
