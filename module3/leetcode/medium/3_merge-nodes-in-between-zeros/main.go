package main

import (
	"fmt"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	return buildLinkList(interArray(head))
}

func buildLinkList(input []int) *ListNode {
	node := &ListNode{
		Val:  0,
		Next: nil,
	}

	prevNode := node
	for i := 1; i < len(input); i++ {
		newNode := &ListNode{
			Val:  input[i],
			Next: nil,
		}
		prevNode.Next = newNode
		prevNode = newNode
	}

	return node
}

func interArray(node *ListNode) []int {
	var res []int
	sum := 0

	for node.Next != nil {
		if node.Next.Val != 0 {
			sum += node.Next.Val
			node = node.Next
			continue
		} else {
			if node.Next.Next == nil {
				break
			}
		}

		res = append(res, sum)
		node = node.Next
		sum = 0
	}

	res = append(res, sum)
	return res
}

func main() {
	nums := []int{0, 1, 0, 3, 0, 2, 2, 0}

	node := buildLinkList(nums)

	fmt.Println(interArray(node))
}
