package main

import (
	"fmt"
	"sort"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func balanceBST(root *TreeNode) *TreeNode {
	list := traverse(root)

	return fillBST(list)

}

func traverse(root *TreeNode) []int {
	var tree []int

	if root == nil {
		return tree
	}

	tree = append(tree, traverse(root.Right)...)
	tree = append(tree, root.Val)
	tree = append(tree, traverse(root.Left)...)

	sort.Ints(tree)

	return tree
}

func fillBST(nums []int) *TreeNode {
	pivot := len(nums) / 2

	node := &TreeNode{
		Val:   nums[pivot],
		Left:  nil,
		Right: nil,
	}

	left := nums[:pivot]
	right := nums[pivot+1:]

	if len(left) > 0 {
		node.Left = fillBST(left)
	}

	if len(right) > 0 {
		node.Right = fillBST(right)
	}

	return node
}

func main() {
	tree := fillBST([]int{1, 2, 3, 4, 5, 6, 7, 8, 9})

	fmt.Println(traverse(tree))
}
