package main

import (
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Реализуйте метод Append для добавления нового поста в конец потока

func (f *Feed) Append(newPost *Post) error {
	if newPost == nil {
		return fmt.Errorf("cannot append nil post")
	}

	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		f.end.next = newPost
		f.end = newPost
	}

	f.length++

	return nil
}

// Реализуйте метод Remove для удаления поста по дате публикации из потока

func (f *Feed) Remove(publishDate int64) error {
	if f.length == 0 {
		return fmt.Errorf("feed has no posts")
	}

	if f.length == 1 && publishDate == f.end.publishDate {
		f.length--

		return nil
	}

	post := f.start
	prevPost := post

	for i := 0; i < f.length; i++ {
		if post.publishDate == publishDate {
			if i == 0 {
				f.start = post.next
				f.length--

				return nil
			}

			if i == f.length-1 {
				f.end = prevPost
				f.length--

				return nil
			}

			prevPost.next = post.next
			f.length--

			return nil
		}

		prevPost = post
		post = post.next
	}

	return fmt.Errorf("post with publication date: %d not found", publishDate)
}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации

func (f *Feed) Insert(newPost *Post) error {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
		f.length++

		return nil
	}

	currentPost := f.start
	prevPost := currentPost

	for i := 0; i < f.length; i++ {
		nextPost := currentPost.next

		if currentPost.publishDate == newPost.publishDate {
			return fmt.Errorf("publication date %d is already occupied", newPost.publishDate)
		}

		if currentPost.publishDate > newPost.publishDate {
			if prevPost == currentPost {
				f.start = newPost
			} else {
				prevPost.next = newPost
			}

			newPost.next = currentPost
			f.length++

			return nil
		}

		prevPost = currentPost
		currentPost = nextPost
	}

	f.end.next = newPost
	f.end = newPost
	f.length++

	return nil
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах

func (f *Feed) Inspect() {
	fmt.Println("Feed length:", f.length)
	//Item: 0: &{Lorem ipsum 1257894000 @xc0@010a020}
	currentPost := f.start
	for i := 0; i < f.length; i++ {
		fmt.Printf("Item: %d: %v\n", i, currentPost)
		currentPost = currentPost.next
	}
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}

	err := f.Append(p1)
	if err != nil {
		return
	}

	err = f.Append(p2)
	if err != nil {
		return
	}

	err = f.Append(p3)
	if err != nil {
		return
	}

	err = f.Append(p4)
	if err != nil {
		return
	}

	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}

	err = f.Insert(newPost)
	if err != nil {
		return
	}

	f.Inspect()

	err = f.Remove(rightNow + 10)

	if err != nil {
		fmt.Println(err)

		return
	}

	f.Inspect()
}
