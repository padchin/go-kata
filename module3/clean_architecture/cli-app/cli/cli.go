package cli

import (
	"bufio"
	"fmt"
	"gitlab.com/padchin/go-kata/module3/clean_architecture/cli-app/service"
	"math/rand"
	"os"
)

type CLI struct {
	todoService *service.TodoService
	scanner     *bufio.Scanner
}

func NewCLI(todoService *service.TodoService) *CLI {
	return &CLI{todoService: todoService, scanner: bufio.NewScanner(os.Stdin)}
}

func (cli *CLI) Run() {
	fmt.Println("Welcome to ToDo application!")

	for {
		fmt.Println()
		fmt.Println("Select an option:")
		fmt.Println("1. Show todos")
		fmt.Println("2. Add a todo")
		fmt.Println("3. Delete a random todo")
		fmt.Println("4. Update a random todo")
		fmt.Println("5. Complete a random todo")
		fmt.Println("6. Exit")

		if !cli.scanner.Scan() {
			break
		}

		option := cli.scanner.Text()

		switch option {
		case "1":
			cli.showTodos()
		case "2":
			cli.addTask()
		case "3":
			cli.deleteTask()
		case "4":
			cli.updateTask()
		case "5":
			cli.completeTask()
		case "6":
			fmt.Println("Fare thee well!")
			return
		default:
			cli.showError("Invalid option selected")
		}
	}
}

func (cli *CLI) showError(text string) {
	fmt.Println(text)
}

func (cli *CLI) updateTask() {
	todos, err := cli.todoService.ListTodos()
	if err != nil {
		cli.showError(err.Error())
	}
	if len(todos) == 0 {
		return
	}
	index := rand.Intn(len(todos))
	id := todos[index].ID
	fmt.Println("updating todo with id:", id)

	todos[index].Title = "updated title"

	err = cli.todoService.UpdateTodo(todos[index])
	if err != nil {
		cli.showError(err.Error())
	}
}

func (cli *CLI) completeTask() {
	todos, err := cli.todoService.ListTodos()
	if err != nil {
		cli.showError(err.Error())
	}
	if len(todos) == 0 {
		return
	}
	index := rand.Intn(len(todos))
	id := todos[index].ID
	fmt.Println("completing todo with id:", id)

	todos[index].Complete = true

	err = cli.todoService.UpdateTodo(todos[index])
	if err != nil {
		cli.showError(err.Error())
	}
}

func (cli *CLI) showTodos() {
	todos, err := cli.todoService.ListTodos()
	if err != nil {
		cli.showError(err.Error())
	}

	for _, v := range todos {
		fmt.Println(v)
	}
}

func (cli *CLI) addTask() {
	task := service.Todo{
		ID:          0,
		Title:       fmt.Sprintf("task #%d", rand.Intn(100)),
		Description: fmt.Sprintf("description #%d", rand.Intn(100)),
		Complete:    false,
	}

	err := cli.todoService.CreateTodo(task)
	if err != nil {
		cli.showError(err.Error())
	}
}

func (cli *CLI) deleteTask() {
	todos, err := cli.todoService.ListTodos()
	if err != nil {
		cli.showError(err.Error())
	}
	if len(todos) == 0 {
		return
	}
	index := rand.Intn(len(todos))
	id := todos[index].ID
	fmt.Println("deleting todo with id:", id)

	err = cli.todoService.RemoveTodo(id)
	if err != nil {
		cli.showError(err.Error())
	}
}
