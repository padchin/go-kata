package main

import (
	"gitlab.com/padchin/go-kata/module3/clean_architecture/cli-app/cli"
	"gitlab.com/padchin/go-kata/module3/clean_architecture/cli-app/repository"
	"gitlab.com/padchin/go-kata/module3/clean_architecture/cli-app/service"
)

func main() {
	repo := repository.NewTaskRepository()
	todoService := service.NewTodoService(repo)
	client := cli.NewCLI(todoService)

	client.Run()
}
