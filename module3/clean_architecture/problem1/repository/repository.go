package repository

import (
	"encoding/json"
	"fmt"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File string
}

func NewUserRepository(file string) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record User) error {
	var usersData []User

	data, err := os.ReadFile(r.File)
	if err == nil {
		if len(data) > 1 {
			err = json.Unmarshal(data, &usersData)
			if err != nil {
				return fmt.Errorf("error unmarshalling data: %s", err.Error())
			}
		}
	}

	exists := false
	for _, v := range usersData {
		if v.ID == record.ID {
			exists = true
			break
		}
	}

	if !exists {
		usersData = append(usersData, record)
	}

	data, err = json.Marshal(&usersData)
	if err != nil {
		return fmt.Errorf("error marshalling data: %s", err.Error())
	}

	err = os.WriteFile(r.File, data, 0644) //nolint:gosec
	if err != nil {
		return fmt.Errorf("error writing data: %s", err.Error())
	}

	return nil
}

func (r *UserRepository) Find(id int) (User, error) {
	data, err := os.ReadFile(r.File)
	if err != nil {
		return User{}, fmt.Errorf("error reading data from file: %s", err.Error())
	}

	var usersData []User
	err = json.Unmarshal(data, &usersData)
	if err != nil {
		return User{}, fmt.Errorf("error unmarshalling data: %s", err.Error())
	}

	for _, v := range usersData {
		if v.ID == id {
			return v, nil
		}
	}

	return User{}, fmt.Errorf("specified user %d not found", id)
}

func (r *UserRepository) FindAll() ([]User, error) {
	data, err := os.ReadFile(r.File)
	if err != nil {
		return nil, fmt.Errorf("error reading data from file: %s", err.Error())
	}

	var usersData []User
	err = json.Unmarshal(data, &usersData)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling data: %s", err.Error())
	}

	return usersData, nil
}
