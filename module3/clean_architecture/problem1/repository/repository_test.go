package repository

import (
	"encoding/json"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	dir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	file, err := os.CreateTemp(dir, "test.*.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	repo := NewUserRepository(file.Name())

	user := User{
		ID:   1,
		Name: "Test User",
	}

	err = repo.Save(user)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	data, err := os.ReadFile(file.Name())
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	var savedUsers []User
	err = json.Unmarshal(data, &savedUsers)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	if len(savedUsers) != 1 || savedUsers[0].ID != user.ID || savedUsers[0].Name != user.Name {
		t.Errorf("Saved user does not match user we saved: expected=%v actual=%v", user, savedUsers[0])
	}

	secondUser := User{
		ID:   1,
		Name: "Second Test User",
	}

	err = repo.Save(secondUser)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	data, err = os.ReadFile(file.Name())
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	var savedUsersAfterConflict []User
	err = json.Unmarshal(data, &savedUsersAfterConflict)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	if len(savedUsersAfterConflict) != 1 || savedUsersAfterConflict[0].ID != user.ID || savedUsersAfterConflict[0].Name != user.Name {
		t.Errorf("Saved users after conflict do not match expected: expected=%v actual=%v", []User{user}, savedUsersAfterConflict)
	}
}

func TestUserRepository_Find(t *testing.T) {
	dir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	file, err := os.CreateTemp(dir, "test.*.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	repo := NewUserRepository(file.Name())

	users := []User{
		{ID: 1, Name: "User One"},
		{ID: 2, Name: "User Two"},
		{ID: 3, Name: "User Three"},
	}

	data, err := json.Marshal(&users)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	err = os.WriteFile(file.Name(), data, 0644)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	for _, user := range users {
		foundUser, err := repo.Find(user.ID)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}

		if foundUser != user {
			t.Errorf("Found user does not match expected: expected=%v actual=%v", user, foundUser)
		}
	}

	nonexistentID := 999
	_, err = repo.Find(nonexistentID)
	if err == nil {
		t.Errorf("Expected error not returned when finding nonexistent user: id=%d", nonexistentID)
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	dir, err := os.MkdirTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	file, err := os.CreateTemp(dir, "test.*.json")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	repo := NewUserRepository(file.Name())

	users := []User{
		{ID: 1, Name: "User One"},
		{ID: 2, Name: "User Two"},
		{ID: 3, Name: "User Three"},
	}

	data, err := json.Marshal(&users)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	err = os.WriteFile(file.Name(), data, 0644)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	foundUsers, err := repo.FindAll()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	if !reflect.DeepEqual(foundUsers, users) {
		t.Errorf("Found users do not match expected: expected=%v actual=%v", users, foundUsers)
	}
}
