package main

import (
	"log"

	"gitlab.com/padchin/go-kata/module3/clean_architecture/problem1/repository"
)

func main() {
	repo := repository.NewUserRepository("repo.json")

	err := repo.Save(repository.User{
		ID:   0,
		Name: "Bob",
	})
	if err != nil {
		log.Fatalf("error adding user: %s", err.Error())
	}
}
