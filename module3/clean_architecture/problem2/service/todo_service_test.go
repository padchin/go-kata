package service

import (
	"encoding/json"
	"gitlab.com/padchin/go-kata/module3/clean_architecture/problem2/repository"
	"os"
	"reflect"
	"testing"
)

func TestTodoService_CompleteTodo(t *testing.T) {
	todos := []Todo{
		{ID: 1, Title: "Todo 1", Description: "", Complete: false},
		{ID: 2, Title: "Todo 2", Description: "", Complete: false},
		{ID: 3, Title: "Todo 3", Description: "", Complete: true},
	}

	data, err := json.Marshal(todos)
	if err != nil {
		t.Fatal(err)
	}
	if err = os.WriteFile("tasks.json", data, 0644); err != nil {
		t.Fatal(err)
	}

	repo := repository.NewTaskRepository()

	s := NewTodoService(repo)

	err = s.CompleteTodo(2)
	if err != nil {
		t.Fatalf("CompleteTodo failed: %v", err)
	}

	updatedTodos, err := s.ListTodos()
	if err != nil {
		t.Fatalf("ListTodos failed: %v", err)
	}

	for _, todo := range updatedTodos {
		if todo.ID == 2 {
			if !todo.Complete {
				t.Fatal("expected todo to be marked as complete, but it wasn't")
			}
			break
		}
	}
}

func TestTodoService_CreateTodo(t *testing.T) {
	todos := []Todo{
		{ID: 1, Title: "Task 1", Description: "Do something", Complete: false},
		{ID: 2, Title: "Task 2", Description: "Do something else", Complete: true},
	}

	data, err := json.Marshal(todos)
	if err != nil {
		t.Fatal(err)
	}
	if err := os.WriteFile("tasks.json", data, 0644); err != nil {
		t.Fatal(err)
	}

	repo := repository.NewTaskRepository()

	s := NewTodoService(repo)

	title := "Test todo"

	task := repository.Task{
		ID:          0,
		Title:       title,
		Description: "do something",
		Complete:    false,
	}

	err = s.CreateTodo(Todo(task))
	if err != nil {
		t.Fatalf("CreateTodo failed: %v", err)
	}

	todos, err = s.ListTodos()
	if err != nil {
		t.Fatalf("ListTodos failed: %v", err)
	}

	if len(todos) != 3 {
		t.Fatalf("expected 3 todo, got %d", len(todos))
	}

	if todos[2].Title != title {
		t.Fatalf("expected todo title to be %q, got %q", title, todos[2].Title)
	}

	if todos[2].Complete {
		t.Fatal("expected todo to be incomplete, but it was marked as complete")
	}
}

func TestTodoService_ListTodos(t *testing.T) {
	todos := []Todo{
		{ID: 1, Title: "Task 1", Description: "Do something", Complete: false},
		{ID: 2, Title: "Task 2", Description: "Do something else", Complete: true},
	}

	data, err := json.Marshal(todos)
	if err != nil {
		t.Fatal(err)
	}

	if err = os.WriteFile("tasks.json", data, 0644); err != nil {
		t.Fatal(err)
	}

	repo := repository.NewTaskRepository()

	s := NewTodoService(repo)

	result, err := s.ListTodos()
	if err != nil {
		t.Errorf("ListTodos() failed with error: %v", err)
	}

	if !reflect.DeepEqual(result, todos) {
		t.Errorf("ListTodos() returned %+v, expected %+v", result, todos)
	}
}

func TestTodoService_RemoveTodo(t *testing.T) {
	todos := []Todo{
		{ID: 1, Title: "Todo 1", Description: "", Complete: false},
		{ID: 2, Title: "Todo 2", Description: "", Complete: false},
		{ID: 3, Title: "Todo 3", Description: "", Complete: true},
	}

	data, err := json.Marshal(todos)
	if err != nil {
		t.Fatal(err)
	}
	if err = os.WriteFile("tasks.json", data, 0644); err != nil {
		t.Fatal(err)
	}

	repo := repository.NewTaskRepository()

	s := NewTodoService(repo)

	err = s.RemoveTodo(2)
	if err != nil {
		t.Fatalf("RemoveTodo failed: %v", err)
	}

	updatedTodos, err := s.ListTodos()
	if err != nil {
		t.Fatalf("ListTodos failed: %v", err)
	}

	for _, todo := range updatedTodos {
		if todo.ID == 2 {
			t.Fatal("expected todo to be removed, but it still exists")
		}
	}

	if len(updatedTodos) != 2 {
		t.Fatal("expected the length of todos to be 2, but it is not")
	}
}
