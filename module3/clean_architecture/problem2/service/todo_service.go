package service

import (
	"fmt"
	"gitlab.com/padchin/go-kata/module3/clean_architecture/problem2/repository"
)

type (
	TodoApp interface {
		ListTodos() ([]Todo, error)
		CreateTodo(title string, task repository.Task) error
		CompleteTodo(id int) error
		RemoveTodo(id int) error
		UpdateTodo(todo Todo) error
	}

	TodoService struct {
		repository repository.TaskRepository
	}

	Todo repository.Task
)

func NewTodoService(repository repository.TaskRepository) *TodoService {
	return &TodoService{repository: repository}
}

func (s *TodoService) ListTodos() ([]Todo, error) {
	todos := make([]Todo, 0)

	tasks, err := s.repository.GetTasks()
	if err != nil {
		return nil, fmt.Errorf("error getting todos list: %s", err.Error())
	}

	for _, task := range tasks {
		todos = append(todos, Todo(task))
	}

	return todos, nil
}

func (s *TodoService) CreateTodo(todo Todo) error {
	err := s.repository.CreateTask(repository.Task(todo))
	if err != nil {
		return fmt.Errorf("error creating task: %s", err.Error())
	}

	return nil
}

func (s *TodoService) CompleteTodo(id int) error {
	todos, err := s.ListTodos()
	if err != nil {
		return fmt.Errorf("error getting todos list: %s", err.Error())
	}

	for i := range todos {
		if todos[i].ID == id {
			todos[i].Complete = true
			err = s.repository.UpdateTask(repository.Task(todos[i]))
			if err != nil {
				return fmt.Errorf("error updating task: %s", err.Error())
			}

			break
		}
	}

	return nil
}

func (s *TodoService) RemoveTodo(id int) error {
	err := s.repository.DeleteTask(id)
	if err != nil {
		return fmt.Errorf("error removing todo: %s", err.Error())
	}

	return nil
}

func (s *TodoService) UpdateTodo(todo Todo) error {
	return s.repository.UpdateTask(repository.Task(todo))
}
