package repository

import (
	"encoding/json"
	"fmt"
	"os"
)

type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Complete    bool   `json:"complete"`
}

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) error
	UpdateTask(task Task) error
	DeleteTask(id int) error
	SaveTasks(tasks []Task) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
}

func (repo *FileTaskRepository) SaveTasks(tasks []Task) error {
	filePath := "tasks.json"
	content, err := json.Marshal(&tasks)
	if err != nil {
		return fmt.Errorf("error marshalling data: %s", err.Error())
	}

	err = os.WriteFile(filePath, content, 0644)
	if err != nil {
		return fmt.Errorf("error writing json file: %s", err.Error())
	}

	return nil
}

func NewTaskRepository() *FileTaskRepository {
	return &FileTaskRepository{}
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	tasks := make([]Task, 0)
	filePath := "tasks.json"

	content, err := os.ReadFile(filePath)
	if err != nil {
		return tasks, nil
	}

	if err = json.Unmarshal(content, &tasks); err != nil {
		return nil, fmt.Errorf("error unmarshalling file contents: %s", err.Error())
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	var task Task

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, nil
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(task Task) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return fmt.Errorf("error getting tasks: %s", err.Error())
	}

	id := 1
	if len(tasks) > 0 {
		id = tasks[len(tasks)-1].ID + 1
	}

	task.ID = id
	tasks = append(tasks, task)

	err = repo.SaveTasks(tasks)

	return err
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task Task) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	err = repo.SaveTasks(tasks)
	if err != nil {
		return err
	}

	return nil
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	for i, t := range tasks {
		if t.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
	}

	err = repo.SaveTasks(tasks)
	if err != nil {
		return fmt.Errorf("error saving tasks: %s", err.Error())
	}

	return nil
}
