package main

import (
	"testing"
	"time"
)

func TestDoubleLinkedList_Reverse(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node1,
		len:  3,
	}

	// Reverse the list
	reversed := dll.Reverse()

	// Check that the reversed list is correct
	switch {
	case reversed == nil:
		t.Errorf("Reverse() returned nil")
	case reversed.len != 3:
		t.Errorf("Reversed list has wrong length, expected 3 but got %d", reversed.len)
	case reversed.head.data.UUID != "789" || reversed.tail.data.UUID != "123":
		t.Errorf("Reversed list has wrong order of nodes")
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node1,
		len:  3,
	}

	// Test search by UUID
	result := dll.SearchUUID("789")
	switch {
	case result == nil:
		t.Errorf("SearchUUID() returned nil")
	case result.data.UUID != "789":
		t.Errorf("SearchUUID() returned wrong commit, expected UUID '789' but got '%s'", result.data.UUID)
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node1,
		len:  3,
	}

	// Test search by UUID
	result := dll.Search("Message B")
	switch {
	case result == nil:
		t.Errorf("Search() returned nil")
	case result.data.Message != "Message B":
		t.Errorf("Search() returned wrong commit, expected message 'Message B' but got '%s'", result.data.Message)
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node1,
		len:  3,
	}

	// Test shifting 3 nodes
	shiftedNode := dll.Shift()
	if shiftedNode != node1 {
		t.Errorf("Shift() returned wrong node")
	}
	if dll.head != node2 {
		t.Errorf("Head node is wrong after Shift()")
	}
	if dll.curr != node2 {
		t.Errorf("Current node is wrong after Shift()")
	}
	if dll.tail != node3 {
		t.Errorf("Tail node is wrong after Shift()")
	}
	if dll.len != 2 {
		t.Errorf("List length is wrong after Shift()")
	}

	// Test shifting 2 nodes
	shiftedNode = dll.Shift()
	if shiftedNode != node2 {
		t.Errorf("Shift() returned wrong node")
	}
	if dll.head != node3 {
		t.Errorf("Head node is wrong after Shift()")
	}
	if dll.tail != node3 {
		t.Errorf("Tail node is wrong after Shift()")
	}
	if dll.curr != node3 {
		t.Errorf("Current node is wrong after Shift()")
	}
	if dll.len != 1 {
		t.Errorf("List length is wrong after Shift()")
	}

	// Test shifting a list with only one node
	shiftedNode = dll.Shift()
	if shiftedNode != node3 {
		t.Errorf("Shift() returned wrong node")
	}
	if dll.head != nil {
		t.Errorf("Head node is wrong after Shift()")
	}
	if dll.tail != nil {
		t.Errorf("Tail node is wrong after Shift()")
	}
	if dll.curr != nil {
		t.Errorf("Current node is wrong after Shift()")
	}
	if dll.len != 0 {
		t.Errorf("List length is wrong after Shift()")
	}

	// Test shifting an empty list
	shiftedNode = dll.Shift()
	if shiftedNode != nil {
		t.Errorf("Shift() returned wrong node")
	}
	if dll.head != nil {
		t.Errorf("Head node is wrong after Shift()")
	}
	if dll.tail != nil {
		t.Errorf("Tail node is wrong after Shift()")
	}
	if dll.curr != nil {
		t.Errorf("Current node is wrong after Shift()")
	}
	if dll.len != 0 {
		t.Errorf("List length is wrong after Shift()")
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node1,
		len:  3,
	}

	// Test Pop() on list with three nodes
	poppedNode := dll.Pop()
	switch {
	case poppedNode == nil:
		t.Errorf("Pop() returned nil")
	case poppedNode.data.UUID != "789":
		t.Errorf("Pop() returned the wrong node")
	case dll.len != 2:
		t.Errorf("Pop() did not update the length of the list correctly")
	case dll.tail.data.UUID != "456":
		t.Errorf("Pop() did not update the tail of the list correctly")
	}

	// list with one node
	dll2 := &DoubleLinkedList{
		head: node1,
		tail: node1,
		curr: node1,
		len:  1,
	}
	poppedNode = dll2.Pop()
	switch {
	case poppedNode == nil:
		t.Errorf("Pop() returned nil")
	case poppedNode.data.UUID != "123":
		t.Errorf("Pop() returned the wrong node")
	case dll2.len != 0:
		t.Errorf("Pop() did not update the length of the list correctly")
	case dll2.head != nil || dll2.tail != nil || dll2.curr != nil:
		t.Errorf("Pop() did not update the head, tail, and current nodes correctly")
	}

	// empty list
	dll3 := &DoubleLinkedList{}
	poppedNode = dll3.Pop()
	switch {
	case poppedNode != nil:
		t.Errorf("Pop() on empty list did not return nil")
	case dll3.len != 0:
		t.Errorf("Pop() on empty list did not update the length of the list correctly")
	case dll3.head != nil || dll3.tail != nil || dll3.curr != nil:
		t.Errorf("Pop() on empty list did not update the head, tail, and current nodes correctly")
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node2,
		len:  3,
	}

	index := dll.Index()

	switch {
	case index != 1:
		t.Errorf("Index() returned the wrong index")
	}

	dll.curr = node1
	index = dll.Index()

	switch {
	case index != 0:
		t.Errorf("Index() returned the wrong index")
	}

	dll.curr = node3
	index = dll.Index()

	switch {
	case index != 2:
		t.Errorf("Index() returned the wrong index")
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node2,
		len:  3,
	}

	// Delete the current node (node2)
	err := dll.DeleteCurrent()
	if err != nil {
		t.Errorf("DeleteCurrent() returned an error: %v", err)
	}
	if dll.head != node1 {
		t.Errorf("Head node is wrong after DeleteCurrent()")
	}
	if dll.curr != node3 {
		t.Errorf("Current node is wrong after DeleteCurrent()")
	}
	if dll.tail != node3 {
		t.Errorf("Tail node is wrong after DeleteCurrent()")
	}
	if dll.len != 2 {
		t.Errorf("List length is wrong after DeleteCurrent()")
	}

	// Delete the current node (node3)
	err = dll.DeleteCurrent()
	if err != nil {
		t.Errorf("DeleteCurrent() returned an error: %v", err)
	}
	if dll.head != node1 {
		t.Errorf("Head node is wrong after DeleteCurrent()")
	}
	if dll.curr != nil {
		t.Errorf("Current node is wrong after DeleteCurrent()")
	}
	if dll.tail != node1 {
		t.Errorf("Tail node is wrong after DeleteCurrent()")
	}
	if dll.len != 1 {
		t.Errorf("List length is wrong after DeleteCurrent()")
	}

	// Delete the current node (node1)
	dll.curr = node1
	err = dll.DeleteCurrent()
	if err != nil {
		t.Errorf("DeleteCurrent() returned an error: %v", err)
	}
	if dll.head != nil {
		t.Errorf("Head node is wrong after DeleteCurrent()")
	}
	if dll.curr != nil {
		t.Errorf("Current node is wrong after DeleteCurrent()")
	}
	if dll.tail != nil {
		t.Errorf("Tail node is wrong after DeleteCurrent()")
	}
	if dll.len != 0 {
		t.Errorf("List length is wrong after DeleteCurrent()")
	}

	// Try to delete a node from an empty list
	err = dll.DeleteCurrent()
	if err == nil {
		t.Errorf("DeleteCurrent() didn't return an error for an empty list")
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	dll := &DoubleLinkedList{
		head: node1,
		tail: node3,
		curr: node1,
		len:  3,
	}

	// deleting the first node
	err := dll.Delete(0)
	if err != nil {
		t.Errorf("Delete returned an error: %v", err)
	}
	if dll.head != node2 {
		t.Errorf("Head node is wrong after Delete(0)")
	}
	if dll.curr != node2 {
		t.Errorf("Current node is wrong after Delete(0)")
	}
	if dll.tail != node3 {
		t.Errorf("Tail node is wrong after Delete(0)")
	}
	if dll.len != 2 {
		t.Errorf("List length is wrong after Delete(0)")
	}

	// deleting the last node
	err = dll.Delete(1)
	if err != nil {
		t.Errorf("Delete returned an error: %v", err)
	}
	if dll.head != node2 {
		t.Errorf("Head node is wrong after Delete(1)")
	}
	if dll.curr != node2 {
		t.Errorf("Current node is wrong after Delete(1)")
	}
	if dll.tail != node2 {
		t.Errorf("Tail node is wrong after Delete(1)")
	}
	if dll.len != 1 {
		t.Errorf("List length is wrong after Delete(1)")
	}

	// deleting the only node in the list
	err = dll.Delete(0)
	if err != nil {
		t.Errorf("Delete returned an error: %v", err)
	}
	if dll.head != nil {
		t.Errorf("Head node is wrong after Delete(0)")
	}
	if dll.curr != nil {
		t.Errorf("Current node is wrong after Delete(0)")
	}
	if dll.tail != nil {
		t.Errorf("Tail node is wrong after Delete(0)")
	}
	if dll.len != 0 {
		t.Errorf("List length is wrong after Delete(0)")
	}

	// deleting from an empty list
	err = dll.Delete(0)
	if err == nil {
		t.Errorf("Delete should have returned an error")
	}
	if dll.head != nil {
		t.Errorf("Head node is wrong after Delete(0) on an empty list")
	}
	if dll.curr != nil {
		t.Errorf("Current node is wrong after Delete(0) on an empty list")
	}
	if dll.tail != nil {
		t.Errorf("Tail node is wrong after Delete(0) on an empty list")
	}
	if dll.len != 0 {
		t.Errorf("List length is wrong after Delete(0) on an empty list")
	}

	// deleting with an out-of-bounds index
	err = dll.Delete(1)
	if err == nil {
		t.Errorf("Delete should have returned an error")
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	// inserting into an empty list
	dll := &DoubleLinkedList{}
	err := dll.Insert(0, Commit{Message: "Message A", UUID: "123", Date: time.Now()})
	if err == nil {
		t.Errorf("Insert() didn't return an error when inserting into an empty list")
	}

	// inserting at negative index
	dll = &DoubleLinkedList{len: 1}
	err = dll.Insert(-1, Commit{Message: "Message B", UUID: "456", Date: time.Now()})
	if err == nil {
		t.Errorf("Insert() didn't return an error when inserting at negative index")
	}

	// inserting at index greater than length
	dll = &DoubleLinkedList{len: 1}
	err = dll.Insert(2, Commit{Message: "Message B", UUID: "456", Date: time.Now()})
	if err == nil {
		t.Errorf("Insert() didn't return an error when inserting at index greater than length")
	}

	// inserting at the beginning
	node1 := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}
	node2 := &Node{
		data: &Commit{Message: "Message B", UUID: "456", Date: time.Now()},
		prev: node1,
		next: nil,
	}
	node3 := &Node{
		data: &Commit{Message: "Message C", UUID: "789", Date: time.Now()},
		prev: node2,
		next: nil,
	}
	node1.next = node2
	node2.next = node3

	currentNode := node1

	dll = &DoubleLinkedList{
		head: node1,
		tail: node1,
		curr: node1,
		len:  1,
	}
	err = dll.Insert(0, *node2.data)
	if err != nil {
		t.Errorf("Insert() returned an error when inserting at the beginning: %v", err)
	}
	if dll.len != 2 {
		t.Errorf("List length is wrong after inserting at the beginning, expected 2 but got %d", dll.len)
	}
	if dll.head != node1 || dll.head.next.data.UUID != "456" {
		t.Errorf("List head is wrong after inserting at the beginning")
	}
	if dll.curr != currentNode {
		t.Errorf("Current node is wrong after inserting at the beginning")
	}

	// inserting at the end

	dll = &DoubleLinkedList{
		head: node1,
		tail: node2,
		curr: node1,
		len:  2,
	}
	err = dll.Insert(1, *node3.data)
	if err != nil {
		t.Errorf("Insert() returned an error when inserting at the end: %v", err)
	}
	if dll.len != 3 {
		t.Errorf("List length is wrong after inserting at the end, expected 3 but got %d", dll.len)
	}
	if dll.curr != node1 {
		t.Errorf("Current node is wrong after inserting at the end")
	}

	// inserting in the middle
	err = dll.Insert(1, Commit{Message: "Message D", UUID: "101112", Date: time.Now()})
	if err != nil {
		t.Errorf("Insert() returned an error: %v", err)
	}
	if dll.len != 4 {
		t.Errorf("List length is wrong after Insert()")
	}
	if dll.curr != currentNode {
		t.Errorf("Current node is wrong after Insert()")
	}
}
