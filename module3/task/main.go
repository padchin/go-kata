package main

import (
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
}

func main() {
	//var list DoubleLinkedList
	//list.LoadData("test.json")
	//node := list.getNodeByIndex(1)
	//fmt.Println(node.data.Message)

	node := &Node{
		data: &Commit{Message: "Message A", UUID: "123", Date: time.Now()},
		prev: nil,
		next: nil,
	}

	dll := &DoubleLinkedList{
		head: node,
		tail: node,
		curr: node,
		len:  1,
	}

	err := dll.Insert(0, Commit{Message: "Message B", UUID: "456", Date: time.Now()})
	if err != nil {
		fmt.Println(err)
	}
	err = dll.Insert(1, Commit{Message: "Message C", UUID: "789", Date: time.Now()})
	if err != nil {
		fmt.Println(err)
	}

	// Reverse the list
	reversed := dll.Reverse()

	_ = reversed

	GenerateJSON()
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	var data []Commit

	err := loadJSON(&data, path)
	if err != nil {
		return fmt.Errorf("error loading JSON: %w", err)
	}

	quickSort(data)

	d.len = len(data)

	var prevNode *Node
	for i, x := range data {
		commit := &Commit{
			Message: x.Message,
			UUID:    x.UUID,
			Date:    x.Date,
		}
		node := new(Node)
		node.data = commit
		node.prev = prevNode

		if i == 0 {
			d.head = node
			d.curr = node
		} else {
			node.prev.next = node
		}

		if i == len(data)-1 {
			node.next = nil
			d.tail = node
		}

		prevNode = node
	}

	return nil
}

func quickSort(data []Commit) {
	if len(data) < 2 {
		return
	}

	pivot := data[rand.Intn(len(data))] //nolint:gosec

	var less, equal, greater []Commit
	for _, x := range data {
		switch {
		case x.Date.Before(pivot.Date):
			less = append(less, x)
		case x.Date.After(pivot.Date):
			greater = append(greater, x)
		default:
			equal = append(equal, x)
		}
	}

	quickSort(less)
	quickSort(greater)

	copy(data[:len(less)], less)
	copy(data[len(less):len(less)+len(equal)], equal)
	copy(data[len(less)+len(equal):], greater)
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	return d.curr.prev
}

func (d *DoubleLinkedList) getNodeByIndex(i int) *Node {
	if i < 0 || i >= d.len {
		return nil
	}

	var curr *Node
	if i <= d.len/2 {
		curr = d.head
		for x := 0; x < i; x++ {
			curr = curr.next
		}
	} else {
		curr = d.tail
		for x := d.len - 1; x > i; x-- {
			curr = curr.prev
		}
	}

	return curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if d.len == 0 {
		return fmt.Errorf("cannot insert into empty list")
	}

	if n < 0 || n > d.len-1 {
		return fmt.Errorf("index out of bounds")
	}

	baseNode := d.getNodeByIndex(n)

	commit := &Commit{
		Message: c.Message,
		UUID:    c.UUID,
		Date:    c.Date,
	}

	node := &Node{
		data: commit,
		prev: baseNode,
		next: nil,
	}

	if n != d.len-1 {
		node.next = baseNode.next
	} else {
		d.tail = node
	}

	baseNode.next = node
	d.len++

	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if d.len == 0 {
		return fmt.Errorf("cannot delete from empty list")
	}

	if n < 0 || n > d.len {
		return fmt.Errorf("index out of bounds")
	}

	curr := d.getNodeByIndex(n)
	if d.curr == curr {
		d.curr = d.curr.next
	}

	switch n {
	case 0:
		if d.len > 1 {
			curr.next.prev = nil
		}
		d.head = curr.next
		if d.len == 1 {
			d.tail = nil
		}
	case d.len - 1:
		curr.prev.next = nil
		d.tail = curr.prev
	default:
		curr.next.prev = curr.prev
		curr.prev.next = curr.next
	}

	d.len--

	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 0 {
		return fmt.Errorf("cannot delete from empty list")
	}

	curr := d.curr
	if curr == nil {
		return fmt.Errorf("current node link is nil")
	}

	switch curr {
	case d.head:
		if d.len > 1 {
			curr.next.prev = nil
		}
		d.head = curr.next
		if d.len == 1 {
			d.tail = nil
		}
	case d.tail:
		curr.prev.next = nil
		d.tail = curr.prev
	default:
		curr.next.prev = curr.prev
		curr.prev.next = curr.next
	}

	d.curr = curr.next
	d.len--

	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() int {
	node := d.curr

	if node.prev == nil {
		return 0
	}

	index := 0
	for node.next != nil {
		index++
		node = node.next
	}

	return d.len - index - 1
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		return nil
	}

	node := d.tail

	if d.len == 1 {
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len--

		return node
	}

	d.tail = node.prev
	d.len--

	return node
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		return nil
	}

	node := d.head

	if d.len == 1 {
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len--

		return node
	}

	d.head = node.next
	d.curr = node.next
	d.len--

	return node
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	node := d.head

	for node != nil {
		if uuID == node.data.UUID {
			return node
		}
		node = node.next
	}

	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	node := d.head

	for node != nil {
		if message == node.data.Message {
			return node
		}
		node = node.next
	}

	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	node := d.tail

	reversed := &DoubleLinkedList{
		head: node,
		tail: node,
		curr: node,
		len:  1,
	}
	node = node.prev
	n := 0

	for node != nil {
		err := reversed.Insert(n, *node.data)
		if err != nil {
			return nil
		}
		n++
		node = node.prev
	}

	return reversed
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON() {
	var dataArray []Commit

	for i := 0; i < 3; i++ {
		date := gofakeit.DateRange(time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC), time.Now())
		data := Commit{
			Message: gofakeit.Sentence(5),
			UUID:    gofakeit.UUID(),
			Date:    date,
		}
		dataArray = append(dataArray, data)
	}

	jsonData, err := json.Marshal(dataArray)
	if err != nil {
		fmt.Println("Error encoding JSON:", err)
		return
	}

	fmt.Println(string(jsonData))
}

func loadJSON(obj interface{}, file string) error {
	jsonBytes, err := os.ReadFile(file)
	if err != nil {
		return err
	}

	err = json.Unmarshal(jsonBytes, obj)
	if err != nil {
		return err
	}

	return nil
}
