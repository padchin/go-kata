package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type (
	WeatherAPI interface {
		GetTemperature(location string) int
		GetHumidity(location string) int
		GetWindSpeed(location string) int
	}

	OpenWeatherAPI struct {
		apiKey string
	}

	WeatherFacade struct {
		weatherAPI WeatherAPI
	}

	WeatherData struct {
		Name string `json:"name"`
		Main struct {
			Temp     float64 `json:"temp"`
			Humidity int     `json:"humidity"`
		} `json:"main"`
		Wind struct {
			Speed float64 `json:"speed"`
		} `json:"wind"`
	}
)

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	wd := getWeatherData(o.apiKey, location)

	return int(wd.Main.Temp)
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	wd := getWeatherData(o.apiKey, location)

	return int(wd.Main.Humidity)
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	wd := getWeatherData(o.apiKey, location)

	return int(wd.Wind.Speed)
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("6e880efd56a20e6ba0422145d5c43464")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}

func getWeatherData(apiKey, city string) WeatherData {
	sbpCityID := "498817"
	encodedCity := url.QueryEscape(city)
	var urlRequest string

	if city == "Санкт-Петербург" {
		urlRequest = fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?id=%s&appid=%s&units=metric", sbpCityID, apiKey)
	} else {
		urlRequest = fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric", encodedCity, apiKey)
	}

	resp, err := http.Get(urlRequest)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	var data WeatherData
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		panic(err)
	}

	return data
}
