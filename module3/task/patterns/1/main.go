package main

import "fmt"

type (
	Order struct {
		Title    string
		Price    float64
		Quantity float64
	}

	PricingStrategy interface {
		Calculate(order Order) float64
	}

	RegularPricing struct {
	}

	SalePricing struct {
		Discount float64
		Type     string
	}

	Strategy struct {
		strategy PricingStrategy
	}
)

const (
	ordinaryCustomer  = "ordinary customer"
	returningCustomer = "returning customer"
	summerSale        = "summer sale"
	megaSale          = "mega sale"
)

func (s *Strategy) setStrategy(strategy PricingStrategy) {
	if s.strategy != strategy {
		s.strategy = strategy
	}
}

func (s *Strategy) Calculate(order Order) float64 {
	return s.strategy.Calculate(order)
}

func (p *SalePricing) Calculate(order Order) float64 {
	return order.Price - order.Price*p.Discount/100.0
}

func (p *RegularPricing) Calculate(order Order) float64 {
	return order.Price
}

func main() {
	strategyList := []SalePricing{{
		Discount: 0,
		Type:     ordinaryCustomer,
	}, {
		Discount: 5,
		Type:     returningCustomer,
	}, {
		Discount: 10,
		Type:     summerSale,
	}, {
		Discount: 15,
		Type:     megaSale,
	},
	}

	goods := []Order{{
		Title:    "Item 1",
		Price:    100,
		Quantity: 3,
	}, {
		Title:    "Item 2",
		Price:    200,
		Quantity: 4,
	}, {
		Title:    "Item 3",
		Price:    300,
		Quantity: 5,
	}}

	strategy := Strategy{}

	for _, s := range strategyList {
		if s.Type == ordinaryCustomer {
			strategy.setStrategy(&RegularPricing{})
		} else {
			strategy.setStrategy(&SalePricing{
				Discount: s.Discount,
				Type:     s.Type,
			})
		}

		sum := 0.0
		for _, g := range goods {
			sum += strategy.Calculate(g)
		}
		fmt.Printf("Total cost with the discount type \"%s\" is $%.2f\n", s.Type, sum)
	}
}
