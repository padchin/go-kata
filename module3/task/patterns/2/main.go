package main

import "fmt"

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type (
	AirConditioner interface {
		TurnOn()
		TurnOff()
		SetTemperature(temp int)
	}
	RealAirConditioner struct {
		AirConditioner
	}

	AirConditionerAdapter struct {
		AirConditioner AirConditioner
	}

	AirConditionerProxy struct {
		permission     bool
		AirConditioner *AirConditionerAdapter
	}
)

func NewRealAirConditioner() *RealAirConditioner {
	return &RealAirConditioner{}
}

func NewAirConditionerProxy(permission bool, airConditioner *AirConditionerAdapter) *AirConditionerProxy {
	return &AirConditionerProxy{permission: permission, AirConditioner: airConditioner}
}

func NewAirConditionerAdapter(airConditioner AirConditioner) *AirConditionerAdapter {
	return &AirConditionerAdapter{AirConditioner: airConditioner}
}

func (r *RealAirConditioner) TurnOn() {
	fmt.Println("ac turned on")
}

func (r *RealAirConditioner) TurnOff() {
	fmt.Println("ac turned off")
}

func (r *RealAirConditioner) SetTemperature(temp int) {
	fmt.Println("ac set to temperature:", temp)
}

func (a *AirConditionerAdapter) TurnOn() {
	a.AirConditioner.TurnOn()
}

func (a *AirConditionerAdapter) TurnOff() {
	a.AirConditioner.TurnOff()
}

func (a *AirConditionerAdapter) SetTemperature(temp int) {
	a.AirConditioner.SetTemperature(temp)
}

func (p *AirConditionerProxy) TurnOn() {
	if !p.permission {
		fmt.Println("not authorized")
	} else {
		p.AirConditioner.TurnOn()
	}
}

func (p *AirConditionerProxy) TurnOff() {
	if !p.permission {
		fmt.Println("not authorized")
	} else {
		p.AirConditioner.TurnOff()
	}
}

func (p *AirConditionerProxy) SetTemperature(temp int) {
	if !p.permission {
		fmt.Println("not authorized")
	} else {
		p.AirConditioner.SetTemperature(temp)
	}
}

func main() {
	ac := NewAirConditionerAdapter(NewRealAirConditioner())

	airConditioner := NewAirConditionerProxy(false, ac) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true, ac) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
