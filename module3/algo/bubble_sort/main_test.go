package main

import (
	"reflect"
	"testing"
)

func TestBubbleSort(t *testing.T) {
	type args struct {
		arr []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reversed array",
			args: args{
				arr: []int{0, 1, 2, 7, 5, 55, 44, 33, 22, 11},
			},
			want: []int{0, 1, 2, 5, 7, 11, 22, 33, 44, 55},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BubbleSort(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BubbleSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkBubbleSort(b *testing.B) {
	arr := []int{0, 1, 2, 7, 5, 55, 44, 33, 22, 11}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = BubbleSort(arr)
	}
}
