package main

func BubbleSort(arr []int) []int {
	i := 0
	swapped := true
	for swapped {
		swapped = false
		for j := 0; j < len(arr)-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
				swapped = true
			}
		}
		i++
	}

	return arr
}
