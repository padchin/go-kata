package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	select {
	case _, ok := <-mergedCh:
		if !ok {
			return mergedCh
		}
	default:
	}

	go func() {
		var wg sync.WaitGroup

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()

				for id := range ch {
					mergedCh <- id
				}
			}(ch, &wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)

		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100): //nolint:gosec
			}
		}
	}()

	return out
}

//nolint:funlen
func main() {
	rand.Seed(time.Now().UnixNano())

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	ctx, cancel := context.WithCancel(context.Background())

	go func(ctx context.Context) {
		defer close(a)

		for num := range out {
			select {
			case <-ctx.Done():
				return
			case a <- num:
			}
		}
	}(ctx)

	go func(ctx context.Context) {
		defer close(b)

		for num := range out {
			select {
			case <-ctx.Done():
				return
			case b <- num:
			}
		}
	}(ctx)

	go func(ctx context.Context) {
		defer close(c)

		for num := range out {
			select {
			case <-ctx.Done():
				return
			case c <- num:
			}
		}
	}(ctx)

	mainChan := joinChannels(a, b, c)
	ticker := time.NewTicker(30000 * time.Millisecond)

	for num := range mainChan {
		select {
		case <-ticker.C:
			cancel()

			break
		default:
			fmt.Println(num)
		}
	}

	fmt.Println("Done!")
}
