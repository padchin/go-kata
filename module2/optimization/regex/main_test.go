package main

import "testing"

func BenchmarkSanitizeText(b *testing.B) {
	b.ResetTimer()

	for t := 0; t < b.N; t++ {
		for i := range data {
			SanitizeText(data[i])
		}
	}
}

func BenchmarkSanitizeText2(b *testing.B) {
	ft := NewFilterText()

	b.ResetTimer()

	for t := 0; t < b.N; t++ {
		for i := range data {
			ft.SanitizeText2(data[i])
		}
	}
}
