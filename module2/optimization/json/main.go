package main

import (
	"encoding/json"
	"fmt"
	jsoniter "github.com/json-iterator/go"
)

var jsonData = []byte(`{
     "ret_code": 0,
     "ret_msg": "OK",
     "ext_code": "",
     "ext_info": "",
     "result": {
         "current_page": 1,
         "data": [
             {
                 "id": 1710,
                 "user_id": 160815,
                 "symbol": "BTCUSDT",
                 "order_id": "e6a11e08-6dd0-404e-bea7-dc22b7ab0228",
                 "side": "Buy",
                 "qty": 0.5,
                 "order_price": 999999,
                 "order_type": "Market",
                 "exec_type": "Trade",
                 "closed_size": 0.5,
                 "cum_entry_value": 3000,
                 "avg_entry_price": 6000,
                 "cum_exit_value": 3000.5,
                 "avg_exit_price": 6001,
                 "closed_pnl": -5.000375,
                 "fill_count": 1,
                 "leverage": 100,
                 "created_at": 1577480599
             }
         ]
     },
     "time_now": "1577480599.097287",
     "rate_limit_status": 119,
     "rate_limit_reset_ms": 1580885703683,
     "rate_limit": 120

}`)

func UnmarshalClosedPositionPNL(data []byte) (ClosedPositionPNL, error) {
	var r ClosedPositionPNL
	err := json.Unmarshal(data, &r)

	return r, err
}

func UnmarshalClosedPositionPNL2(data []byte) (ClosedPositionPNL, error) {
	var r ClosedPositionPNL

	var json1 = jsoniter.ConfigCompatibleWithStandardLibrary

	err := json1.Unmarshal(data, &r)

	return r, err
}

func (r *ClosedPositionPNL) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func (r *ClosedPositionPNL) Marshal2() ([]byte, error) {
	var json1 = jsoniter.ConfigCompatibleWithStandardLibrary
	return json1.Marshal(r)
}

type ClosedPositionPNL struct {
	RetCode          int64  `json:"ret_code"`
	RetMsg           string `json:"ret_msg"`
	EXTCode          string `json:"ext_code"`
	EXTInfo          string `json:"ext_info"`
	Result           Result `json:"result"`
	TimeNow          string `json:"time_now"`
	RateLimitStatus  int64  `json:"rate_limit_status"`
	RateLimitResetMS int64  `json:"rate_limit_reset_ms"`
	RateLimit        int64  `json:"rate_limit"`
}

type Result struct {
	CurrentPage int64   `json:"current_page"`
	Data        []Datum `json:"data"`
}

type Datum struct {
	ID            int64   `json:"id"`
	UserID        int64   `json:"user_id"`
	Symbol        string  `json:"symbol"`
	OrderID       string  `json:"order_id"`
	Side          string  `json:"side"`
	Qty           float64 `json:"qty"`
	OrderPrice    int64   `json:"order_price"`
	OrderType     string  `json:"order_type"`
	ExecType      string  `json:"exec_type"`
	ClosedSize    float64 `json:"closed_size"`
	CumEntryValue int64   `json:"cum_entry_value"`
	AvgEntryPrice int64   `json:"avg_entry_price"`
	CumExitValue  float64 `json:"cum_exit_value"`
	AvgExitPrice  int64   `json:"avg_exit_price"`
	ClosedPnl     float64 `json:"closed_pnl"`
	FillCount     int64   `json:"fill_count"`
	Leverage      int64   `json:"leverage"`
	CreatedAt     int64   `json:"created_at"`
}

func main() {
	s, _ := UnmarshalClosedPositionPNL(jsonData)

	fmt.Printf("%+v", s)
}
