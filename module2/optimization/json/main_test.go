package main

import "testing"

func BenchmarkStandardJson(b *testing.B) {
	var (
		cpp  ClosedPositionPNL
		err  error
		data []byte
	)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		cpp, err = UnmarshalClosedPositionPNL(jsonData)

		if err != nil {
			panic(err)
		}

		data, err = cpp.Marshal()

		if err != nil {
			panic(err)
		}

		_ = data
	}
}

func BenchmarkJsonIterator(b *testing.B) {
	var (
		cpp  ClosedPositionPNL
		err  error
		data []byte
	)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		cpp, err = UnmarshalClosedPositionPNL2(jsonData)

		if err != nil {
			panic(err)
		}

		data, err = cpp.Marshal2()

		if err != nil {
			panic(err)
		}

		_ = data
	}
}
