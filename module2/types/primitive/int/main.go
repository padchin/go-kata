package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")

	var uint8Number uint8 = 1 << 7
	min8 := int8(uint8Number)
	uint8Number--
	max8 := int8(uint8Number)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, "size:", unsafe.Sizeof(uint8Number), "bytes")

	var uint16Number uint16 = 1 << 15
	min16 := int16(uint16Number)
	uint16Number--
	max16 := int16(uint16Number)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, "size:", unsafe.Sizeof(uint16Number), "bytes")

	var uint32Number uint32 = 1 << 31
	min32 := int32(uint32Number)
	uint32Number--
	max32 := int32(uint32Number)
	fmt.Println("int32 min value:", min32, "max32 max value:", max32, "size:", unsafe.Sizeof(uint32Number), "bytes")

	var uint64Number uint64 = 1 << 63
	min64 := int64(uint64Number)
	uint64Number--
	max64 := int64(uint64Number)
	fmt.Println("min64 min value:", min64, "max64 max value:", max64, "size:", unsafe.Sizeof(uint64Number), "bytes")

	fmt.Println("=== END type int ===")
}
