package main

import "fmt"

func main() {
	numberInt := 3

	numberFloat := float32(numberInt)
	fmt.Printf("type: %T, value: %v", numberFloat, numberFloat)
}
