package main

import "fmt"

// Исправь функцию Append, чтобы изменения повлияли на массив в функции main, решение должно вывести четыре значения в консоль.

func main() {
	s := []int{1, 2, 3}
	Append(&s)
	fmt.Println(s)
}

func Append(s *[]int) {
	*s = append(*s, 4)
}
