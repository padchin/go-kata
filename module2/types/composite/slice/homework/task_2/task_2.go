package main

import "fmt"

// Удали пользователей возрастом выше 40 лет из слайса, выведи результат в консоль

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}

	newSlice := make([]User, 0)

	for _, v := range users {
		if v.Age <= 40 {
			newSlice = append(newSlice, v)
		}
	}

	users = newSlice

	fmt.Println(users)
}
