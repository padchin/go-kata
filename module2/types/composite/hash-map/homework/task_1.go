package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{"https://github.com/golang/go", 109269},
		{"https://github.com/kubernetes/kubernetes", 96552},
		{"https://github.com/tensorflow/tensorflow", 172017},
		{"https://github.com/ansible/ansible", 56633},
		{"https://github.com/prometheus/prometheus", 47133},
		{"https://github.com/elastic/elasticsearch", 62992},
		{"https://github.com/docker/docker-ce", 5587},
		{"https://github.com/apache/kafka", 24369},
		{"https://github.com/moby/moby", 65452},
		{"https://github.com/istio/istio", 32575},
		{"https://github.com/openai/gpt-2", 17622},
		{"https://github.com/llvm/llvm-project", 18561},
		{"https://github.com/pytorch/pytorch", 63622},
	}

	// в цикле запишите в map
	hashMap := make(map[string]int)
	for _, v := range projects {
		hashMap[v.Name] = v.Stars
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for k, v := range hashMap {
		fmt.Println(k, "-", v)
	}
}
