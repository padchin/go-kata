package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	defer func() {
		job.IsFinished = true
	}()

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}

	for _, dict := range job.Dicts {
		if dict == nil {
			return job, errNilDict
		}

		for k, v := range dict {
			job.Merged[k] = v
		}
	}

	return job, nil
}

func main() {
	jobs := make([]*MergeDictsJob, 3)

	// valid struct
	jobs[0] = &MergeDictsJob{
		Dicts: []map[string]string{
			{"a": "b", "b": "c"},
			{"d": "e", "f": "g"},
			{"h": "k", "l": "m"},
		},
		Merged:     make(map[string]string),
		IsFinished: false,
	}

	// not valid struct
	jobs[1] = &MergeDictsJob{
		Dicts: []map[string]string{
			{"a": "b", "b": "c"},
			{"d": "e", "f": "g"},
			nil,
			{"h": "k", "l": "m"},
		},
		Merged:     make(map[string]string),
		IsFinished: false,
	}

	// not valid struct
	jobs[2] = &MergeDictsJob{
		Dicts:      []map[string]string{},
		Merged:     make(map[string]string),
		IsFinished: false,
	}

	for _, j := range jobs {
		result, err := ExecuteMergeDictsJob(j)
		if result.IsFinished { // always should be true (just to demonstrate the result of "defer")
			if err != nil {
				fmt.Printf("error: %v\n", err)
				continue
			}
			fmt.Printf("%+v\n", result.Merged)
		}
	}
}
