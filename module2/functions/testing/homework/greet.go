package main

import "fmt"

func Greet(name string) string {
	if isRussian(name) {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
	return fmt.Sprintf("Hello %s, you welcome!", name)
}

func isRussian(s string) bool {
	for _, r := range s {
		if r < 'А' || r > 'я' {
			return false
		}
	}
	return true
}

func main() {
	print(Greet("Gopher"))
}
