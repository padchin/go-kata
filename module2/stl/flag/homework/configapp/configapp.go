package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	conf := new(Config)

	confPath := flag.String("conf", "", "path to configuration file")
	flag.Parse()

	if *confPath == "" {
		panic(fmt.Sprintf("Usage: %s -conf config.json\n", os.Args[0]))
	}

	err := JSONLoad(conf, *confPath)
	if err != nil {
		panic(fmt.Sprintf("error loading config.json: %v", err))
	}

	fmt.Printf("%+v", conf)
}

func JSONLoad(obj interface{}, file string) error {
	jsonBytes, err := os.ReadFile(file)
	if err != nil {
		return err
	}
	err = json.Unmarshal(jsonBytes, obj)
	if err != nil {
		return err
	}
	return nil
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
