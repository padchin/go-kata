package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, _ := sql.Open("sqlite3", "gopher.db")
	st, _ := db.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, _ = st.Exec()

	st, _ = db.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, _ = st.Exec("Sarah", "Connor")
	_, _ = st.Exec("Dart", "Vader")
	_, _ = st.Exec("Han", "Solo")

	rows, _ := db.Query("SELECT id, firstname, lastname FROM people")
	var id int
	var firstName, lastName string
	for rows.Next() {
		_ = rows.Scan(&id, &firstName, &lastName)
		fmt.Printf("id %d: First name: %s, Last name: %s\n", id, firstName, lastName)
	}
}
