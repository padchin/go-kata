package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	// Open the file for reading
	file, err := os.Open("/tmp/dat")
	check(err)
	defer file.Close()

	// Read the entire contents of the file into a byte slice
	fileContent, err := io.ReadAll(file)
	check(err)
	fmt.Print(string(fileContent))

	// Seek to the beginning of the file
	_, err = file.Seek(0, 0)
	check(err)

	// Read the first 5 bytes of the file into a byte slice
	buffer1 := make([]byte, 5)
	numRead1, err := file.Read(buffer1)
	check(err)
	fmt.Printf("%d bytes: %s\n", numRead1, string(buffer1[:numRead1]))

	// Seek to the 6th byte in the file and read the next 2 bytes
	offset2, err := file.Seek(6, 0)
	check(err)
	buffer2 := make([]byte, 2)
	numRead2, err := file.Read(buffer2)
	check(err)
	fmt.Printf("%d bytes @ %d: ", numRead2, offset2)
	fmt.Printf("%v\n", string(buffer2[:numRead2]))

	// Seek to the 6th byte in the file and read at least 2 bytes
	offset3, err := file.Seek(6, 0)
	check(err)
	buffer3 := make([]byte, 2)
	numRead3, err := io.ReadAtLeast(file, buffer3, 2)
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", numRead3, offset3, string(buffer3))

	// Seek back to the beginning of the file and read the first 5 bytes using a buffered reader
	_, err = file.Seek(0, 0)
	check(err)
	reader4 := bufio.NewReader(file)
	buffer4, err := reader4.Peek(5)
	check(err)
	fmt.Printf("5 bytes: %s\n", string(buffer4))
}
