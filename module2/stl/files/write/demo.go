package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func Transliteration(s string) string {
	r := strings.NewReplacer(
		"а", "a",
		"б", "b",
		"в", "v",
		"г", "g",
		"д", "d",
		"е", "je",
		"ё", "jo",
		"ж", "zh",
		"з", "z",
		"и", "i",
		"й", "j",
		"к", "k",
		"л", "l",
		"м", "m",
		"н", "n",
		"о", "o",
		"п", "p",
		"р", "r",
		"с", "s",
		"т", "t",
		"у", "u",
		"ф", "f",
		"х", "kh",
		"ц", "ts",
		"ч", "ch",
		"ш", "sh",
		"щ", "sch",
		"ъ", "",
		"ы", "y",
		"ь", "",
		"э", "e",
		"ю", "ju",
		"я", "ja",
		"А", "A",
		"Б", "B",
		"В", "V",
		"Г", "G",
		"Д", "D",
		"Е", "Je",
		"Ё", "Jo",
		"Ж", "Zh",
		"З", "Z",
		"И", "I",
		"Й", "J",
		"К", "K",
		"Л", "L",
		"М", "M",
		"Н", "N",
		"О", "O",
		"П", "P",
		"Р", "R",
		"С", "S",
		"Т", "T",
		"У", "U",
		"Ф", "F",
		"Х", "Kh",
		"Ц", "Ts",
		"Ч", "Ch",
		"Ш", "Sh",
		"Щ", "Sch",
		"Ъ", "",
		"Ы", "Y",
		"Ь", "",
		"Э", "E",
		"Ю", "Ju",
		"Я", "Ja",
	)
	return r.Replace(s)
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your name: ")
	name, _ := reader.ReadString('\n')
	err := os.WriteFile("name.txt", []byte(name), 0644)
	if err != nil {
		panic(err)
	}

	fileContent, err := os.ReadFile("example.txt")
	if err != nil {
		panic(err)
	}

	trans := Transliteration(string(fileContent))

	file2, err := os.Create("example.processed.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file2.Close()

	_, err = file2.WriteString(trans)
	if err != nil {
		panic(err)
	}

	err = file2.Sync()
	if err != nil {
		return
	}
}
