package main

import (
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {
	generateSelfStory("Vlad", 25, 10.00000025)
}

func generateSelfStory(name string, age int, funds float64) {
	fmt.Printf("Hello! My name is %s. I'm %d y.o. And I also have $%0.2f in my wallet right now.", name, age, funds)
}
