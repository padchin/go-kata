package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	var b bytes.Buffer

	// запишите данные в буфер
	_, _ = b.WriteString(strings.Join(data, "\n"))

	// создайте файл
	f, _ := os.Create("example.txt")
	// запишите данные в файл
	writer := bufio.NewWriter(f)
	_, _ = writer.Write(b.Bytes())
	_ = writer.Flush()
	_ = f.Sync()
	_ = f.Close()

	// прочтите данные в новый буфе
	var buffer bytes.Buffer
	f, _ = os.Open("example.txt")
	defer func(f *os.File) {
		_ = f.Close()
	}(f)
	_, _ = io.Copy(&buffer, f)

	// выведите данные из буфера buffer.String()
	// у вас все получится!
	scanner := bufio.NewScanner(&buffer)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

// Note that error handling is intentionally omitted to simplify the code.
