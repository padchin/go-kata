package main

import (
	"math"
	"testing"
)

func Test_multiply(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test1", args: args{a: 2, b: 3}, want: 6},
		{name: "Test2", args: args{a: 0, b: 5}, want: 0},
		{name: "Test3", args: args{a: -1, b: -3}, want: 3},
		{name: "Test4", args: args{a: 4.2, b: 2.5}, want: 10.5},
		{name: "Test5", args: args{a: -5, b: 0.5}, want: -2.5},
		{name: "Test6", args: args{a: 10, b: -0.5}, want: -5},
		{name: "Test7", args: args{a: 1e+15, b: 2.5}, want: 2.5e+15},
		{name: "Test8", args: args{a: 1e-15, b: 1e-15}, want: 1e-30},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("multiply() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_divide(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test1", args: args{a: 10, b: 5}, want: 2},
		{name: "Test2", args: args{a: 15, b: 0}, want: math.Inf(1)},
		{name: "Test3", args: args{a: -6, b: 3}, want: -2},
		{name: "Test4", args: args{a: 2.5, b: 4.2}, want: 0.5952380952380952},
		{name: "Test5", args: args{a: 0, b: 5}, want: 0},
		{name: "Test6", args: args{a: -9, b: -3}, want: 3},
		{name: "Test7", args: args{a: 1.2345e+15, b: 2.5}, want: 4.938e+14},
		{name: "Test8", args: args{a: 1e-15, b: 1e-12}, want: 1e-3},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("divide() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sum(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test1", args: args{a: 6, b: 3}, want: 9},
		{name: "Test2", args: args{a: -5, b: 5}, want: 0},
		{name: "Test3", args: args{a: 100, b: 200}, want: 300},
		{name: "Test4", args: args{a: 10.5, b: 2.75}, want: 13.25},
		{name: "Test5", args: args{a: -15.6, b: 5.3}, want: -10.3},
		{name: "Test6", args: args{a: 0, b: 0}, want: 0},
		{name: "Test7", args: args{a: 1e+20, b: -1e+20}, want: 0},
		{name: "Test8", args: args{a: 1.5e-10, b: 2.5e-10}, want: 4e-10},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("sum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_average(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "Test1", args: args{a: 2, b: 4}, want: 3},
		{name: "Test2", args: args{a: -5, b: -15}, want: -10},
		{name: "Test3", args: args{a: 7.5, b: 2.5}, want: 5},
		{name: "Test4", args: args{a: 0, b: 0}, want: 0},
		{name: "Test5", args: args{a: 100, b: -100}, want: 0},
		{name: "Test6", args: args{a: 0.5, b: 0.5}, want: 0.5},
		{name: "Test7", args: args{a: 1, b: 1}, want: 1},
		{name: "Test8", args: args{a: -1, b: 1}, want: 0},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("average() = %v, want %v", got, tt.want)
			}
		})
	}
}
