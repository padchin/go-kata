// Исправь код так, чтобы программа вывела текст Success!. Для решения задачи нужно использовать type-switch.
// В функции test надо определить,  является ли интерфейсное значение nil.
package main

import (
	"fmt"
)

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch t := r.(type) {
	case *int:
		if t == nil {
			fmt.Println("Success!")
		}
	}
}
