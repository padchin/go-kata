package main

import (
	"bytes"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Add a new pet to the store.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
//
//lint:ignore U1000
type petAddRequest struct {
	// Pet object that needs to be added to the store
	// in:body
	Body model.Pet
}

// swagger:response petAddResponse
//
//lint:ignore U1000
type petAddResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/{petId} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse
//	 404: notFoundResponse

// swagger:parameters petGetByIDRequest
//
//lint:ignore U1000
type petGetByIDRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"petId"`
}

// swagger:response petGetByIDResponse
//
//lint:ignore U1000
type petGetByIDResponse struct {
	// in:body
	Body model.Pet
}

// swagger:route PUT /pet pet petUpdateRequest
// Update an existing pet.
// This endpoint updates an existing pet with the provided ID.
//
// Responses:
//
//	400: badRequestResponse
//	404: notFoundResponse
//	405: validationExceptionResponse
//
// swagger:parameters petUpdateRequest
//
//lint:ignore U1000
type petUpdateRequest struct {
	// The ID of the pet to update.
	//
	// Required: true
	// in: path
	ID int64 `json:"petId"`

	// The updated information for the pet.
	//
	// Required: true
	// in: body
	Body model.Pet
}

// swagger:response badRequestResponse
// Invalid ID supplied.
//
//lint:ignore U1000
type badRequestResponse struct {
	// A message indicating that the provided ID is invalid.
	//
	// in: body
	Body struct {
		Message string `json:"message"`
	}
}

// swagger:response notFoundResponse
// Pet not found.
//
//lint:ignore U1000
type notFoundResponse struct {
	// A message indicating that the pet with the provided ID was not found.
	//
	// in: body
	Body struct {
		Message string `json:"message"`
	}
}

// swagger:response validationExceptionResponse
// Validation exception occurred.
//
//lint:ignore U1000
type validationExceptionResponse struct {
	// A message indicating that a validation exception occurred.
	//
	// in: body
	Body struct {
		Message string `json:"message"`
	}
}

// swagger:route POST /pet/{petId} pet updatePetRequest
// Updates a pet in the store with form data.
//
// Responses:
//
//	200: updatePetResponse
//	400: badRequestResponse
//	404: notFoundResponse
//	405: validationExceptionResponse
//
// swagger:parameters updatePetRequest
//
//lint:ignore U1000
type updatePetRequest struct {
	// ID of pet to update
	//
	// in: path
	PetID int64 `json:"petId"`

	// Name of pet
	//
	// in: formData
	Name string `json:"name"`

	// Status of pet
	//
	// in: formData
	Status string `json:"status"`
}

// swagger:response updatePetResponse
//
//lint:ignore U1000
type updatePetResponse struct {
	// in: body
	Body model.Pet
}

// swagger:route DELETE /pet/{petId} pet deletePetRequest
// Deletes a pet.
//
// Responses:
//
//	200: deletePetResponse
//	400: badRequestResponse
//	404: notFoundResponse
//	405: validationExceptionResponse
//
// swagger:parameters deletePetRequest
//
//lint:ignore U1000
type deletePetRequest struct {
	// ID of pet to delete.
	//
	// in: path
	PetID int64 `json:"petId"`
}

// swagger:response deletePetResponse
//
//lint:ignore U1000
type deletePetResponse struct {
	// in: body
	Body struct {
		// A message indicating that the pet has been deleted.
		Message string `json:"message"`
	}
}

// swagger:route POST /pet/{petId}/uploadImage pet petUploadImage
//
// Upload an image for a pet.
//
// Responses:
//   400: errorResponse
//   404: errorResponse
//   500: errorResponse

// swagger:parameters petUploadImage
//
//lint:ignore U1000
type petUploadImage struct {
	// Upload image file.
	//
	// in: formData
	//
	// swagger:file
	Image *bytes.Buffer `json:"image"`
	// ID of pet to delete.
	//
	// in: path
	PetID int64 `json:"petId"`
}
