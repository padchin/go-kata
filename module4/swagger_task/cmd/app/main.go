package main

import (
	"context"
	"fmt"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/controller"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

//nolint:funlen
func main() {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Current directory:", cwd)

	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petController := controller.NewPetController()
	storeController := controller.NewStoreController()
	userController := controller.NewUserController()

	r.Post("/pet/{petID}/uploadImage", petController.PetUploadImage)
	r.Get("/pet/findByStatus/{status}", petController.PetFindByStatus)
	r.Post("/pet", petController.PetCreate)
	r.Get("/pet/{petID}", petController.PetGetByID)
	r.Put("/pet/", petController.PetUpdate)
	r.Post("/pets/{petID}", petController.PetUpdateWithFormData)
	r.Delete("/pets/{petID}", petController.PetDelete)

	r.Post("/store", storeController.StoreCreate)
	r.Get("/store/{storeID}", storeController.StoreGetByID)
	r.Delete("/store/{storeID}", storeController.StoreDelete)
	r.Get("/store/inventory", storeController.GetInventory)

	r.Post("/user", userController.UserCreate)
	r.Get("/user/{username}", userController.UserGetByName)
	r.Delete("/user/{username}", userController.UserDelete)
	r.Put("/user/{username}", userController.UserUpdate)
	r.Post("/user/createWithList", userController.UserCreateWithList)

	// SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{ //nolint:gosec
		Addr:    port,
		Handler: r,
	}

	go func() {
		log.Printf("server started on port %s\n", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
