package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/repository"
	"net/http"
	"strconv"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

type StoreController struct {
	storage *repository.StoreStorage
}

func NewStoreController() *StoreController {
	return &StoreController{storage: repository.NewStoreStorage()}
}

// swagger:route POST /store store createStore
//
// Create a new store.
//
// responses:
//   200: storeResponse
//   400: errorResponse

// swagger:response storeResponse
//
//lint:ignore U1000
type storeResponse struct {
	// in: body
	Body model.Store
}

// swagger:response errorResponse
//
//lint:ignore U1000
type errorResponse struct {
	// in: body
	Body struct {
		// Error message
		Message string `json:"message"`
	}
}

// swagger:parameters createStore
//
//lint:ignore U1000
type createStore struct {
	// Pet object that needs to be added to the store
	// in:body
	Body model.Store
}

func (s *StoreController) StoreCreate(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var store model.Store
	err := json.NewDecoder(r.Body).Decode(&store)

	if err != nil {
		http.Error(w, fmt.Sprintf("error decoding request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	validate := validator.New()
	if err := validate.Struct(store); err != nil {
		http.Error(w, fmt.Sprintf("invalid request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	store = s.storage.Create(store) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(&store)                          // записываем результат Store json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

// swagger:route GET /store/{storeID} store getStoreByID
//
// Get a store by ID.
//
// Responses:
//
//	200: storeResponse
//	404: errorResponse
//	400: errorResponse
//
// swagger:parameters getStoreByID
//
//lint:ignore U1000
type getStoreByID struct {
	// ID of a store
	//
	// in: path
	StoreID string `json:"storeID"`
}

func (s *StoreController) StoreGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		store      model.Store
		err        error
		storeIDRaw string
		storeID    int
	)

	storeIDRaw = chi.URLParam(r, "storeID")

	storeID, err = strconv.Atoi(storeIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	store, err = s.storage.GetByID(storeID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// swagger:route DELETE /store/{storeID} store storeDelete
//
// Deletes a store by ID.
//
// Responses:
//
//	200: storeResponse
//	404: errorResponse
//	400: errorResponse
//
// swagger:parameters storeDelete
//
//lint:ignore U1000
type storeDelete struct {
	// ID of a store
	//
	// in: path
	StoreID string `json:"storeID"`
}

func (s *StoreController) StoreDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodDelete {
		http.Error(w, "invalid request method", http.StatusMethodNotAllowed)
		return
	}

	storeID, err := strconv.Atoi(chi.URLParam(r, "storeID"))
	if err != nil {
		http.Error(w, "invalid store ID", http.StatusBadRequest)
		return
	}

	err = s.storage.Delete(storeID)

	if err != nil {
		http.Error(w, "store not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// swagger:route GET /store/inventory store getInventory
// Returns pet inventories by status.
//
// Produces:
// - application/json
//
// Responses:
//   200: inventoryResponse

func (s *StoreController) GetInventory(w http.ResponseWriter, r *http.Request) {
	inventory := s.storage.GetInventoryByStatus()
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(inventory)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// swagger:response inventoryResponse
//
//lint:ignore U1000
type inventoryResponse struct {
	// in: body
	Body Inventory
}

// Inventory represents the current inventory status
type Inventory map[string]int
