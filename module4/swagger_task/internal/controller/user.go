package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/repository"
	"net/http"
)

type UserController struct {
	storage *repository.UserStorage
}

func NewUserController() *UserController {
	controller := &UserController{storage: repository.NewUserStorage()}
	return controller
}

// swagger:route POST /user user createUser
//
// Create a new user.
//
// Responses:
//
//	200: userResponse
//	400: badRequestResponse
//	405: methodNotAllowedResponse
//
// swagger:parameters createUser
//
//lint:ignore U1000
type createUser struct {
	// User information.
	//
	// in: body
	Body model.User
}

// swagger:response userResponse
//
//lint:ignore U1000
type userResponse struct {
	// User information.
	//
	// in: body
	Body model.User
}

// swagger:response badRequestResponse
//
//lint:ignore U1000
type badRequestResponse struct {
	// Error message.
	//
	// in: body
	Body struct {
		// Description of the error.
		//
		// Required: true
		Description string `json:"description"`
	}
}

// swagger:response methodNotAllowedResponse
//
//lint:ignore U1000
type methodNotAllowedResponse struct {
	// Error message.
	//
	// in: body
	Body struct {
		// Description of the error.
		//
		// Required: true
		Description string `json:"description"`
	}
}

func (s *UserController) UserCreate(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, fmt.Sprintf("error decoding request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	validate := validator.New()
	if err := validate.Struct(user); err != nil {
		http.Error(w, fmt.Sprintf("invalid request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	user = s.storage.Create(user)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// swagger:route GET /user/{username} user getUserByName
//
// Get a user by username.
//
// Responses:
//
//	200: userResponse
//	404: errorResponse
//	400: errorResponse
//
// swagger:parameters getUserByName
//
//lint:ignore U1000
type getUserByName struct {
	// User information.
	//
	// in: path
	Username string `json:"username"`
}

func (s *UserController) UserGetByName(w http.ResponseWriter, r *http.Request) {
	var (
		user     *model.User
		err      error
		username string
	)

	username = chi.URLParam(r, "username")

	user, err = s.storage.GetByName(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// swagger:route DELETE /user/{username} user userDelete
//
// Deletes a user by username.
//
// Responses:
//
//	200: userResponse
//	404: errorResponse
//	400: errorResponse
//	405: methodNotAllowedResponse
//
// swagger:parameters userDelete
//
//lint:ignore U1000
type userDelete struct {
	// User information.
	//
	// in: path
	Username string `json:"username"`
}

func (s *UserController) UserDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodDelete {
		http.Error(w, "invalid request method", http.StatusMethodNotAllowed)
		return
	}

	username := chi.URLParam(r, "username")

	err := s.storage.Delete(username)

	if err != nil {
		http.Error(w, "user not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// swagger:route PUT /user/{username} user userUpdate
//
// Updates a user by username.
//
// Responses:
//
//	200: userResponse
//	404: errorResponse
//	400: errorResponse
//
// swagger:parameters userUpdate
//
//lint:ignore U1000
type userUpdate struct {
	// User information.
	//
	// in: path
	Username string `json:"username"`
	// in: body
	Body model.User
}

func (s *UserController) UserUpdate(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")

	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, fmt.Sprintf("error decoding request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	_, err = s.storage.Update(username, user)

	if err != nil {
		http.Error(w, "user not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// swagger:route POST /user/createWithList user userCreateWithList
//
// Creates the list of users by the given users list.
//
// Responses:
//
//		200: userResponse
//		404: errorResponse
//		400: errorResponse
//	    500: errorResponse
//		405: methodNotAllowedResponse
//
// swagger:parameters userCreateWithList
//
//lint:ignore U1000
type userCreateWithList struct {
	// Users list.
	//
	// in: body
	Body []model.User
}

func (s *UserController) UserCreateWithList(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "invalid request method", http.StatusMethodNotAllowed)
		return
	}

	var users []*model.User
	err := json.NewDecoder(r.Body).Decode(&users)
	if err != nil {
		http.Error(w, fmt.Sprintf("error decoding request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	err = s.storage.CreateWithList(users)

	if err != nil {
		http.Error(w, "error creating users", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
