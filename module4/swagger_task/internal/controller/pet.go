package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/repository"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

type PetController struct {
	storage *repository.PetStorage
}

func NewPetController() *PetController {
	return &PetController{storage: repository.NewPetStorage()}
}

func (p *PetController) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet)

	if err != nil {
		http.Error(w, fmt.Sprintf("error decoding request: %s", err.Error()), http.StatusBadRequest)
		return
	}

	pet = p.storage.Create(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(&pet)                            // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		pet      model.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.GetByID(petID) // пытаемся получить Pet по id
	if err != nil {                     // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.Update(pet) // создаем запись в нашем storage
	if err != nil {                  // отправляем 304 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusNotModified)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(&pet)                            // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetFindByStatus(w http.ResponseWriter, r *http.Request) {
	status := chi.URLParam(r, "status")

	if !(status == "pending" || status == "sold" || status == "available") {
		http.Error(w, "invalid status value", http.StatusBadRequest)
		return
	}

	pets, err := p.storage.GetByStatus(status)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&pets)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetUpdateWithFormData(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "invalid request method", http.StatusMethodNotAllowed)
		return
	}

	petIDRaw := chi.URLParam(r, "petID")
	petID, err := strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, "invalid pet ID", http.StatusBadRequest)
		return
	}

	err = r.ParseForm()
	if err != nil {
		http.Error(w, "error parsing form data", http.StatusBadRequest)
		return
	}

	name := r.FormValue("name")
	status := r.FormValue("status")

	pet, err := p.storage.GetByID(petID)
	if err != nil {
		http.Error(w, "error obtaining pet from storage", http.StatusInternalServerError)
		return
	}

	if name != "" {
		pet.Name = name
	}

	if status != "" {
		pet.Status = status
	}

	pet, err = p.storage.Update(pet)
	if err != nil {
		http.Error(w, "error updating pet in storage", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&pet)
	if err != nil {
		http.Error(w, "error encoding pet as JSON", http.StatusInternalServerError)
		return
	}
}

func (p *PetController) PetDelete(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodDelete {
		http.Error(w, "invalid request method", http.StatusMethodNotAllowed)
		return
	}

	petID, err := strconv.Atoi(chi.URLParam(r, "petID"))
	if err != nil {
		http.Error(w, "invalid pet ID", http.StatusBadRequest)
		return
	}

	deletedPet := p.storage.Delete(petID)

	if deletedPet == nil {
		http.Error(w, "pet not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (p *PetController) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	_ = os.MkdirAll("uploads", 0644)

	petIDRaw := chi.URLParam(r, "petID")

	petID, err := strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err := p.storage.GetByID(petID)
	if err != nil {
		http.Error(w, "pet not found", http.StatusNotFound)
		return
	}

	err = r.ParseMultipartForm(10 << 20) // max file size 10MB
	if err != nil {
		http.Error(w, "error parsing request", http.StatusBadRequest)
		return
	}

	file, fileHeader, err := r.FormFile("image")
	if err != nil {
		http.Error(w, "error reading file from request", http.StatusBadRequest)
		return
	}
	defer file.Close()

	filename := fmt.Sprintf("%d_%d_%s", petID, time.Now().Unix(), filepath.Base(fileHeader.Filename))
	path := filepath.Join("uploads", filename)
	out, err := os.Create(path)
	if err != nil {
		http.Error(w, fmt.Sprintf("error saving file to disk: %s", err.Error()), http.StatusInternalServerError)
		return
	}

	defer out.Close()
	_, err = io.Copy(out, file)
	if err != nil {
		http.Error(w, fmt.Sprintf("error saving file to disk: %s", err.Error()), http.StatusInternalServerError)
		return
	}

	pet.PhotoUrls = append(pet.PhotoUrls, fmt.Sprintf("uploads/%s", filename))

	_, err = p.storage.Update(pet)
	if err != nil {
		http.Error(w, "error updating pet", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(&pet)
	if err != nil {
		http.Error(w, "error encoding response", http.StatusInternalServerError)
		return
	}
}
