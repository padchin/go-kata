package repository

import (
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	f "gitlab.com/padchin/go-kata/module4/swagger_task/pkg/repository"
	"os"
	"reflect"
	"sync"
	"testing"
)

func TestGetByStatus(t *testing.T) {
	defer func() {
		_ = os.Remove(petFile)
	}()

	t.Run("EmptyPetStorage", func(t *testing.T) {
		petStorage := &PetStorage{}
		_, err := petStorage.GetByStatus("available")
		if err == nil {
			t.Errorf("Expected error, but got nil")
		}
	})

	t.Run("NoMatch", func(t *testing.T) {
		petStorage := &PetStorage{
			primaryKeyIDx: map[int64]*model.Pet{
				1: {
					ID:     1,
					Name:   "Dog",
					Status: "sold",
				},
				2: {
					ID:     2,
					Name:   "Cat",
					Status: "pending",
				},
			},
			data: []*model.Pet{
				{
					ID:     1,
					Name:   "Dog",
					Status: "sold",
				},
				{
					ID:     2,
					Name:   "Cat",
					Status: "pending",
				},
			},
		}
		_, err := petStorage.GetByStatus("available")
		if err == nil {
			t.Errorf("Expected error, but got nil")
		}
	})

	t.Run("OneMatch", func(t *testing.T) {
		petStorage := &PetStorage{
			primaryKeyIDx: map[int64]*model.Pet{
				1: {
					ID:     1,
					Name:   "Dog",
					Status: "sold",
				},
				2: {
					ID:     2,
					Name:   "Cat",
					Status: "pending",
				},
			},
			data: []*model.Pet{
				{
					ID:     1,
					Name:   "Dog",
					Status: "sold",
				},
				{
					ID:     2,
					Name:   "Cat",
					Status: "pending",
				},
			},
		}
		pets, err := petStorage.GetByStatus("sold")
		if err != nil {
			t.Errorf("Expected nil, but got error: %v", err)
		}
		if len(pets) != 1 {
			t.Errorf("Expected 1 pet, but got %d", len(pets))
		}
		if pets[0].ID != 1 {
			t.Errorf("Expected pet with ID 1, but got pet with ID %d", pets[0].ID)
		}
	})

	t.Run("MultipleMatches", func(t *testing.T) {
		petStorage := &PetStorage{
			primaryKeyIDx: map[int64]*model.Pet{
				1: {
					ID:     1,
					Name:   "Dog",
					Status: "sold",
				},
				2: {
					ID:     2,
					Name:   "Cat",
					Status: "sold",
				},
			},
			data: []*model.Pet{
				{
					ID:     1,
					Name:   "Dog",
					Status: "sold",
				},
				{
					ID:     2,
					Name:   "Cat",
					Status: "sold",
				},
			},
		}
		pets, err := petStorage.GetByStatus("sold")
		if err != nil {
			t.Errorf("Expected nil, but got error: %v", err)
		}
		if len(pets) != 2 {
			t.Errorf("Expected 2 pets, but got %d", len(pets))
		}
	})
}

func TestPetStorage_Create(t *testing.T) {
	defer func() {
		_ = os.Remove(petFile)
	}()

	petStorage := &PetStorage{
		data:          []*model.Pet{},
		primaryKeyIDx: make(map[int64]*model.Pet),
	}

	newPet := model.Pet{
		Category:  model.PetCategory{Name: "Cat"},
		Name:      "Garfield",
		PhotoUrls: []string{"https://example.com/cat.jpg"},
		Tags:      []model.PetCategory{{Name: "Fat"}, {Name: "Lazy"}},
		Status:    "available",
	}
	createdPet := petStorage.Create(newPet)

	if createdPet.ID != 0 {
		t.Errorf("expected ID to be 0, got %v", createdPet.ID)
	}
	if len(petStorage.data) != 1 {
		t.Errorf("expected PetStorage length to be 1, got %v", len(petStorage.data))
	}
	if petStorage.autoIncrementCount != 1 {
		t.Errorf("expected autoIncrementCount to be 1, got %v", petStorage.autoIncrementCount)
	}

	retrievedPet, ok := petStorage.primaryKeyIDx[createdPet.ID]
	if !ok {
		t.Errorf("expected Pet with ID %v to be in primaryKeyIDx map", createdPet.ID)
	}
	if !reflect.DeepEqual(retrievedPet, &createdPet) {
		t.Errorf("expected retrieved Pet to be the same as created Pet")
	}

	var petsFromJSON []*model.Pet
	err := f.LoadJSON(&petsFromJSON, petFile)
	if err != nil {
		t.Fatalf("error loading pets from JSON: %v", err)
	}
	if len(petsFromJSON) != 1 {
		t.Errorf("expected pets from JSON length to be 1, got %v", len(petsFromJSON))
	}
	if petsFromJSON[0].ID != createdPet.ID {
		t.Errorf("expected ID in JSON to be %v, got %v", createdPet.ID, petsFromJSON[0].ID)
	}
}

func TestPetStorage_Update(t *testing.T) {
	defer func() {
		_ = os.Remove(petFile)
	}()

	addPets := func(petStorage *PetStorage, pets []model.Pet) {
		for _, pet := range pets {
			_ = petStorage.Create(pet)
		}
	}

	t.Run("update existing pet", func(t *testing.T) {
		petStorage := NewPetStorage()

		testPets := []model.Pet{
			{
				ID:        1,
				Category:  model.PetCategory{Name: "Dog"},
				Name:      "Fido",
				PhotoUrls: []string{"https://example.com/dog.jpg"},
				Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Active"}},
				Status:    "available",
			},
			{
				ID:        2,
				Category:  model.PetCategory{Name: "Cat"},
				Name:      "Garfield",
				PhotoUrls: []string{"https://example.com/cat.jpg"},
				Tags:      []model.PetCategory{{Name: "Fat"}, {Name: "Lazy"}},
				Status:    "available",
			},
		}
		addPets(petStorage, testPets)

		updatedPet := model.Pet{
			ID:        1,
			Category:  model.PetCategory{Name: "Dog"},
			Name:      "Spot",
			PhotoUrls: []string{"https://example.com/dog.jpg"},
			Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Energetic"}},
			Status:    "sold",
		}
		_, err := petStorage.Update(updatedPet)
		if err != nil {
			t.Fatalf("error updating Pet: %v", err)
		}

		if pet := petStorage.primaryKeyIDx[1]; pet == nil {
			t.Errorf("expected updated Pet to be in primaryKeyIDx map")
		} else {
			if pet.Name != "Spot" {
				t.Errorf("expected Pet name to be 'Spot', got %q", pet.Name)
			}
			if len(pet.Tags) != 2 || pet.Tags[1].Name != "Energetic" {
				t.Errorf("expected Pet tags to be [{Name: 'Friendly'}, {Name: 'Energetic'}], got %v", pet.Tags)
			}
			if pet.Status != "sold" {
				t.Errorf("expected Pet status to be 'sold', got %q", pet.Status)
			}
		}

		var petsFromJSON []*model.Pet
		err = f.LoadJSON(&petsFromJSON, petFile)
		if err != nil {
			t.Fatalf("error loading pets from JSON: %v", err)
		}
		if len(petsFromJSON) != 2 {
			t.Errorf("expected pets from JSON length to be 2, got %v", len(petsFromJSON))
		}
		if petsFromJSON[1].ID != 1 || petsFromJSON[1].Name != "Spot" {
			t.Errorf("expected updated Pet to be at index 0 in JSON, got %v", petsFromJSON)
		}
	})

	t.Run("update non-existent pet", func(t *testing.T) {
		petStorage := NewPetStorage()

		// try to update a pet that doesn't exist
		updatedPet := model.Pet{
			ID:        3,
			Category:  model.PetCategory{Name: "Dog"},
			Name:      "Spot",
			PhotoUrls: []string{"https://example.com/dog.jpg"},
			Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Energetic"}},
			Status:    "sold",
		}
		_, err := petStorage.Update(updatedPet)

		// expect an error to be returned
		if err == nil {
			t.Errorf("expected error updating non-existent Pet")
		}
	})
}

func TestPetStorage_Delete(t *testing.T) {
	defer func() {
		_ = os.Remove(petFile)
	}()

	addPets := func(petStorage *PetStorage, pets []model.Pet) {
		for _, pet := range pets {
			_ = petStorage.Create(pet)
		}
	}

	petStorage := NewPetStorage()

	testPets := []model.Pet{
		{
			ID:        1,
			Category:  model.PetCategory{Name: "Dog"},
			Name:      "Fido",
			PhotoUrls: []string{"https://example.com/dog.jpg"},
			Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Active"}},
			Status:    "available",
		},
		{
			ID:        2,
			Category:  model.PetCategory{Name: "Cat"},
			Name:      "Garfield",
			PhotoUrls: []string{"https://example.com/cat.jpg"},
			Tags:      []model.PetCategory{{Name: "Fat"}, {Name: "Lazy"}},
			Status:    "available",
		},
		{
			ID:        3,
			Category:  model.PetCategory{Name: "Cat"},
			Name:      "Tom",
			PhotoUrls: []string{"https://example.com/cat.jpg"},
			Tags:      []model.PetCategory{{Name: "Fat"}, {Name: "Lazy"}},
			Status:    "available",
		},
		{
			ID:        4,
			Category:  model.PetCategory{Name: "Dog"},
			Name:      "Pluto",
			PhotoUrls: []string{"https://example.com/dog.jpg"},
			Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Active"}},
			Status:    "available",
		},
	}

	addPets(petStorage, testPets)

	petID := 2

	err := petStorage.Delete(petID)
	if err != nil {
		t.Errorf("Error deleting pet: %v", err)
	}
	if len(petStorage.data) != 3 {
		t.Errorf("Expected number of pets after deletion: 3, actual: %d", len(petStorage.data))
	}
	if _, ok := petStorage.primaryKeyIDx[int64(petID)]; ok {
		t.Errorf("Pet with ID %d should be deleted from the primaryKeyIDx map", petID)
	}

	petID = 6
	err = petStorage.Delete(petID)
	if err == nil {
		t.Errorf("Expected an error while deleting a pet that doesn't exist")
	}
}

func TestPetStorage_GetByID(t *testing.T) {
	defer func() {
		_ = os.Remove(petFile)
	}()

	addPets := func(petStorage *PetStorage, pets []model.Pet) {
		for _, pet := range pets {
			_ = petStorage.Create(pet)
		}
	}

	petStorage := NewPetStorage()

	testPets := []model.Pet{
		{
			ID:        0,
			Category:  model.PetCategory{Name: "Dog"},
			Name:      "Fido",
			PhotoUrls: []string{"https://example.com/dog.jpg"},
			Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Active"}},
			Status:    "available",
		},
		{
			ID:        1,
			Category:  model.PetCategory{Name: "Cat"},
			Name:      "Garfield",
			PhotoUrls: []string{"https://example.com/cat.jpg"},
			Tags:      []model.PetCategory{{Name: "Fat"}, {Name: "Lazy"}},
			Status:    "available",
		},
		{
			ID:        2,
			Category:  model.PetCategory{Name: "Cat"},
			Name:      "Tom",
			PhotoUrls: []string{"https://example.com/cat.jpg"},
			Tags:      []model.PetCategory{{Name: "Fat"}, {Name: "Lazy"}},
			Status:    "available",
		},
		{
			ID:        3,
			Category:  model.PetCategory{Name: "Dog"},
			Name:      "Pluto",
			PhotoUrls: []string{"https://example.com/dog.jpg"},
			Tags:      []model.PetCategory{{Name: "Friendly"}, {Name: "Active"}},
			Status:    "available",
		},
	}

	addPets(petStorage, testPets)

	gotPet, err := petStorage.GetByID(0)
	if err != nil {
		t.Errorf("GetByID() error = %v, want %v", err, nil)
	}
	if !reflect.DeepEqual(gotPet, testPets[0]) {
		t.Errorf("GetByID() gotPet = %v, want %v", gotPet, testPets[0])
	}

	_, err = petStorage.GetByID(6)
	if err == nil || err.Error() != "not found" {
		t.Errorf("GetByID() error = %v, want %v", err, "not found")
	}

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			_, _ = petStorage.GetByID(1)
			_, _ = petStorage.GetByID(2)
			_, _ = petStorage.GetByID(3)
		}()
	}
	wg.Wait()
}
