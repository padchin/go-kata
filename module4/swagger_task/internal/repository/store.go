package repository

import (
	"fmt"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	f "gitlab.com/padchin/go-kata/module4/swagger_task/pkg/repository"
	"sync"
)

const storeFile = "store_data.json"

type StoreStorager interface {
	Create(store model.Store) model.Store
	Delete(storeID int) error
	GetByID(storeID int) (model.Store, error)
	GetInventoryByStatus() map[string]int
}

type StoreStorage struct {
	data               []*model.Store
	primaryKeyIDx      map[int64]*model.Store
	autoIncrementCount int64
	sync.RWMutex
}

func NewStoreStorage() *StoreStorage {
	data := make([]*model.Store, 0)
	_ = f.LoadJSON(&data, storeFile)

	primaryKeyIDx := make(map[int64]*model.Store)
	for i, v := range data {
		primaryKeyIDx[int64(i)] = v
	}

	var autoIncrementCount int64
	if len(data) > 0 {
		autoIncrementCount = int64(data[len(data)-1].ID + 1)
	}

	return &StoreStorage{
		autoIncrementCount: autoIncrementCount,
		data:               data,
		primaryKeyIDx:      primaryKeyIDx,
	}
}

func (s *StoreStorage) Create(store model.Store) model.Store {
	s.Lock()
	defer s.Unlock()

	_ = f.LoadJSON(&s.data, storeFile)

	for _, v := range s.data {
		s.primaryKeyIDx[int64(v.ID)] = v
	}

	store.ID = int(s.autoIncrementCount)
	s.primaryKeyIDx[int64(store.ID)] = &store
	s.autoIncrementCount++
	s.data = append(s.data, &store)

	_ = f.DumpJSON(&s.data, storeFile)

	return store
}

func (s *StoreStorage) Delete(storeID int) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.primaryKeyIDx[int64(storeID)]; !ok {
		return fmt.Errorf("store not found")
	}

	s.data = append(s.data[:storeID], s.data[storeID+1:]...)
	delete(s.primaryKeyIDx, int64(storeID))

	_ = f.DumpJSON(&s.data, storeFile)

	return nil
}

func (s *StoreStorage) GetByID(storeID int) (model.Store, error) {
	s.RLock()
	defer s.RUnlock()

	if v, ok := s.primaryKeyIDx[int64(storeID)]; ok {
		return *v, nil
	}

	return model.Store{}, fmt.Errorf("not found")
}

func (s *StoreStorage) GetInventoryByStatus() map[string]int {
	result := make(map[string]int)

	for _, pet := range s.data {
		if _, ok := result[pet.Status]; !ok {
			result[pet.Status] = 1
		} else {
			result[pet.Status]++
		}
	}

	return result
}
