package repository

import (
	"fmt"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	f "gitlab.com/padchin/go-kata/module4/swagger_task/pkg/repository"
	"sync"
)

const userFile = "users_data.json"

type UserStorager interface {
	Create(user model.User) model.User
	Update(username string, user model.User) (model.User, error)
	Delete(username string) error
	GetByName(username string) (*model.User, error)
	CreateWithList(users []*model.User) error
}

type UserStorage struct {
	data               []*model.User
	primaryKeyIDx      map[int64]*model.User
	autoIncrementCount int64
	sync.RWMutex
}

func NewUserStorage() *UserStorage {
	data := make([]*model.User, 0)
	primaryKeyIDx := make(map[int64]*model.User)
	_ = f.LoadJSON(&data, userFile)

	for i, v := range data {
		primaryKeyIDx[int64(i)] = v
	}

	var autoIncrementCount int64
	if len(data) > 0 {
		autoIncrementCount = int64(data[len(data)-1].ID + 1)
	}

	return &UserStorage{
		autoIncrementCount: autoIncrementCount,
		data:               data,
		primaryKeyIDx:      primaryKeyIDx,
	}
}

func (s *UserStorage) Create(user model.User) model.User {
	s.Lock()
	defer s.Unlock()

	data := make([]*model.User, 0)
	_ = f.LoadJSON(&data, userFile)

	for _, v := range s.data {
		s.primaryKeyIDx[int64(v.ID)] = v
	}

	user.ID = int(s.autoIncrementCount)
	s.data = append(s.data, &user)
	s.primaryKeyIDx[int64(user.ID)] = &user

	s.autoIncrementCount++

	_ = f.DumpJSON(&s.data, userFile)

	return user
}

func (s *UserStorage) Update(username string, user model.User) (model.User, error) {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.primaryKeyIDx[int64(user.ID)]; !ok {
		return model.User{}, fmt.Errorf("user with ID %d does not exist", user.ID)
	}

	for i, u := range s.data {
		if u.Username == username {
			id := u.ID
			user.ID = id
			s.data[i] = &user
			s.primaryKeyIDx[int64(user.ID)] = &user
			break
		}
	}

	_ = f.DumpJSON(&s.data, userFile)

	return user, nil
}

func (s *UserStorage) Delete(username string) error {
	s.Lock()
	defer s.Unlock()

	for i, user := range s.data {
		if user.Username == username {
			s.data = append(s.data[:i], s.data[i+1:]...)
			delete(s.primaryKeyIDx, int64(user.ID))
			_ = f.DumpJSON(&s.data, userFile)

			return nil
		}
	}

	return fmt.Errorf("not found")
}

func (s *UserStorage) GetByName(username string) (*model.User, error) {
	s.RLock()
	defer s.RUnlock()

	for _, user := range s.data {
		if user.Username == username {
			return user, nil
		}
	}

	return nil, fmt.Errorf("user with name %s does not exist", username)
}

func (s *UserStorage) CreateWithList(users []*model.User) error {
	s.Lock()
	defer s.Unlock()

	_ = f.LoadJSON(&s.data, userFile)

	for _, user := range users {
		user.ID = int(s.autoIncrementCount)
		s.data = append(s.data, user)
		s.primaryKeyIDx[int64(user.ID)] = user
		s.autoIncrementCount++
	}

	err := f.DumpJSON(&s.data, userFile)

	return err
}
