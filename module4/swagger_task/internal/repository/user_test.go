package repository

import (
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	f "gitlab.com/padchin/go-kata/module4/swagger_task/pkg/repository"
	"os"
	"reflect"
	"testing"
)

func TestUserStorage_Create(t *testing.T) {
	defer func() {
		_ = os.Remove(userFile)
	}()

	userStorage := &UserStorage{
		data:          []*model.User{},
		primaryKeyIDx: make(map[int64]*model.User),
	}

	newUser := model.User{
		Username:  "user1",
		FirstName: "John",
		LastName:  "Doe",
		Email:     "johndoe@example.com",
		Password:  "password1",
		Phone:     "1234567890",
	}

	createdUser := userStorage.Create(newUser)

	if createdUser.ID != 0 {
		t.Errorf("expected ID to be 0, got %v", createdUser.ID)
	}
	if len(userStorage.data) != 1 {
		t.Errorf("expected StoreStorage length to be 1, got %v", len(userStorage.data))
	}
	if userStorage.autoIncrementCount != 1 {
		t.Errorf("expected autoIncrementCount to be 1, got %v", userStorage.autoIncrementCount)
	}

	retrievedUser, ok := userStorage.primaryKeyIDx[int64(createdUser.ID)]
	if !ok {
		t.Errorf("expected Store with ID %v to be in primaryKeyIDx map", createdUser.ID)
	}
	if !reflect.DeepEqual(retrievedUser, &createdUser) {
		t.Errorf("expected retrieved Store to be the same as created Store")
	}

	var usersFromJSON []*model.User
	err := f.LoadJSON(&usersFromJSON, userFile)
	if err != nil {
		t.Fatalf("error loading stores from JSON: %v", err)
	}
	if len(usersFromJSON) != 1 {
		t.Errorf("expected stores from JSON length to be 1, got %v", len(usersFromJSON))
	}
	if usersFromJSON[0].ID != createdUser.ID {
		t.Errorf("expected ID in JSON to be %v, got %v", createdUser.ID, usersFromJSON[0].ID)
	}
}

func TestUserStorage_CreateWithList(t *testing.T) {
	defer func() {
		_ = os.Remove(userFile)
	}()

	s := &UserStorage{
		data:               []*model.User{},
		primaryKeyIDx:      make(map[int64]*model.User),
		autoIncrementCount: 0,
	}

	users := []*model.User{
		{
			Username:  "user1",
			FirstName: "John",
			LastName:  "Doe",
			Email:     "johndoe@example.com",
			Password:  "password1",
			Phone:     "1234567890",
		},
		{
			Username:  "user2",
			FirstName: "Jane",
			LastName:  "Doe",
			Email:     "janedoe@example.com",
			Password:  "password2",
			Phone:     "0987654321",
		},
	}

	err := s.CreateWithList(users)
	if err != nil {
		t.Fatalf("Error creating users: %v", err)
	}

	if len(s.data) != len(users) {
		t.Errorf("Expected data slice length %d, but got %d", len(users), len(s.data))
	}
	for _, user := range users {
		if s.primaryKeyIDx[int64(user.ID)] != user {
			t.Errorf("Expected primary key index value %v, but got %v", user, s.primaryKeyIDx[int64(user.ID)])
		}
	}
}

func TestUserStorage_Delete(t *testing.T) {
	defer func() {
		_ = os.Remove(userFile)
	}()

	s := &UserStorage{
		data:               []*model.User{},
		primaryKeyIDx:      make(map[int64]*model.User),
		autoIncrementCount: 0,
	}

	users := []*model.User{
		{
			Username:  "user1",
			FirstName: "John",
			LastName:  "Doe",
			Email:     "johndoe@example.com",
			Password:  "password1",
			Phone:     "1234567890",
		},
		{
			Username:  "user2",
			FirstName: "Jane",
			LastName:  "Doe",
			Email:     "janedoe@example.com",
			Password:  "password2",
			Phone:     "0987654321",
		},
		{
			Username:  "user3",
			FirstName: "Bob",
			LastName:  "Doe",
			Email:     "bobdoe@example.com",
			Password:  "password3",
			Phone:     "55555555",
		},
	}
	_ = s.CreateWithList(users)

	err := s.Delete("user2")
	if err != nil {
		t.Fatalf("Error deleting user: %v", err)
	}
	if len(s.data) != len(users)-1 {
		t.Errorf("Expected data slice length %d, but got %d", len(users)-1, len(s.data))
	}
	if _, ok := s.primaryKeyIDx[1]; ok {
		t.Errorf("Expected primary key index value to be deleted, but it still exists")
	}

	// Delete a non-existing user and check if it returns an error
	err = s.Delete("user4")
	if err == nil {
		t.Error("Expected error when deleting non-existing user, but got nil")
	}
}

func TestUserStorage_GetByName(t *testing.T) {
	defer func() {
		_ = os.Remove(userFile)
	}()

	s := &UserStorage{
		data:               []*model.User{},
		primaryKeyIDx:      make(map[int64]*model.User),
		autoIncrementCount: 0,
	}

	users := []*model.User{
		{
			Username:  "user1",
			FirstName: "John",
			LastName:  "Doe",
			Email:     "johndoe@example.com",
			Password:  "password1",
			Phone:     "1234567890",
		},
		{
			Username:  "user2",
			FirstName: "Jane",
			LastName:  "Doe",
			Email:     "janedoe@example.com",
			Password:  "password2",
			Phone:     "0987654321",
		},
	}
	_ = s.CreateWithList(users)

	user, err := s.GetByName("user1")
	if err != nil {
		t.Fatalf("Error getting user: %v", err)
	}
	if user.Username != "user1" {
		t.Errorf("Expected username %s, but got %s", "user1", user.Username)
	}

	_, err = s.GetByName("user3")
	if err == nil {
		t.Error("Expected error when getting non-existing user, but got nil")
	}
}

func TestUserStorage_Update(t *testing.T) {
	defer func() {
		_ = os.Remove(userFile)
	}()
	s := &UserStorage{
		data:               []*model.User{},
		primaryKeyIDx:      make(map[int64]*model.User),
		autoIncrementCount: 0,
	}

	// Create a test user to be added to the data slice
	user := &model.User{
		Username:  "user1",
		FirstName: "John",
		LastName:  "Doe",
		Email:     "johndoe@example.com",
		Password:  "password1",
		Phone:     "1234567890",
	}
	_ = s.CreateWithList([]*model.User{user})

	// Update the user's email and check if the returned user is correct
	user.Email = "newemail@example.com"
	updatedUser, err := s.Update(user.Username, *user)
	if err != nil {
		t.Fatalf("Error updating user: %v", err)
	}
	if updatedUser.Email != "newemail@example.com" {
		t.Errorf("Expected email %s, but got %s", "newemail@example.com", updatedUser.Email)
	}

	// Update a non-existing user and check if it returns an error
	nonExistingUser := &model.User{
		ID:        100,
		Username:  "user100",
		FirstName: "Non-existing",
		LastName:  "User",
		Email:     "nonexistinguser@example.com",
		Password:  "password100",
		Phone:     "5555555555",
	}
	_, err = s.Update("", *nonExistingUser)
	if err == nil {
		t.Error("Expected error when updating non-existing user, but got nil")
	}
}
