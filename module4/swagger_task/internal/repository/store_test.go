package repository

import (
	"github.com/go-playground/assert/v2"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	f "gitlab.com/padchin/go-kata/module4/swagger_task/pkg/repository"
	"os"
	"reflect"
	"sync"
	"testing"
	"time"
)

func TestStoreStorage_Create(t *testing.T) {
	defer func() {
		_ = os.Remove(storeFile)
	}()

	storeStorage := &StoreStorage{
		data:          []*model.Store{},
		primaryKeyIDx: make(map[int64]*model.Store),
	}

	newStore := model.Store{
		ID:       0,
		PetID:    0,
		Quantity: 0,
		ShipDate: time.Time{},
		Status:   "",
		Complete: false,
	}

	createdStore := storeStorage.Create(newStore)

	if createdStore.ID != 0 {
		t.Errorf("expected ID to be 0, got %v", createdStore.ID)
	}
	if len(storeStorage.data) != 1 {
		t.Errorf("expected StoreStorage length to be 1, got %v", len(storeStorage.data))
	}
	if storeStorage.autoIncrementCount != 1 {
		t.Errorf("expected autoIncrementCount to be 1, got %v", storeStorage.autoIncrementCount)
	}

	retrievedStore, ok := storeStorage.primaryKeyIDx[int64(createdStore.ID)]
	if !ok {
		t.Errorf("expected Store with ID %v to be in primaryKeyIDx map", createdStore.ID)
	}
	if !reflect.DeepEqual(retrievedStore, &createdStore) {
		t.Errorf("expected retrieved Store to be the same as created Store")
	}

	var storesFromJSON []*model.Store
	err := f.LoadJSON(&storesFromJSON, storeFile)
	if err != nil {
		t.Fatalf("error loading stores from JSON: %v", err)
	}
	if len(storesFromJSON) != 1 {
		t.Errorf("expected stores from JSON length to be 1, got %v", len(storesFromJSON))
	}
	if storesFromJSON[0].ID != createdStore.ID {
		t.Errorf("expected ID in JSON to be %v, got %v", createdStore.ID, storesFromJSON[0].ID)
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	defer func() {
		_ = os.Remove(storeFile)
	}()

	addStores := func(storeStorage *StoreStorage, stores []model.Store) {
		for _, store := range stores {
			_ = storeStorage.Create(store)
		}
	}

	storeStorage := NewStoreStorage()

	testStores := []model.Store{
		{
			ID:       0,
			PetID:    0,
			Quantity: 1,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
		{
			ID:       1,
			PetID:    2,
			Quantity: 1,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
		{
			ID:       2,
			PetID:    4,
			Quantity: 2,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
		{
			ID:       3,
			PetID:    6,
			Quantity: 1,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
	}

	addStores(storeStorage, testStores)

	storeID := 2

	err := storeStorage.Delete(storeID)
	if err != nil {
		t.Errorf("Error deleting store: %v", err)
	}
	if len(storeStorage.data) != 3 {
		t.Errorf("Expected number of stores after deletion: 3, actual: %d", len(storeStorage.data))
	}
	if _, ok := storeStorage.primaryKeyIDx[int64(storeID)]; ok {
		t.Errorf("Store with ID %d should be deleted from the primaryKeyIDx map", storeID)
	}

	storeID = 6
	err = storeStorage.Delete(storeID)
	if err == nil {
		t.Errorf("Expected an error while deleting a store that doesn't exist")
	}
}

func TestStoreStorage_GetByID(t *testing.T) {
	defer func() {
		_ = os.Remove(storeFile)
	}()

	addStores := func(storeStorage *StoreStorage, stores []model.Store) {
		for _, store := range stores {
			_ = storeStorage.Create(store)
		}
	}

	storeStorage := NewStoreStorage()

	testStores := []model.Store{
		{
			ID:       0,
			PetID:    0,
			Quantity: 1,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
		{
			ID:       1,
			PetID:    2,
			Quantity: 1,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
		{
			ID:       2,
			PetID:    4,
			Quantity: 2,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
		{
			ID:       3,
			PetID:    6,
			Quantity: 1,
			ShipDate: time.Time{},
			Status:   "",
			Complete: false,
		},
	}

	addStores(storeStorage, testStores)

	gotStore, err := storeStorage.GetByID(0)
	if err != nil {
		t.Errorf("GetByID() error = %v, want %v", err, nil)
	}
	if !reflect.DeepEqual(gotStore, testStores[0]) {
		t.Errorf("GetByID() gotStore = %v, want %v", gotStore, testStores[0])
	}

	_, err = storeStorage.GetByID(6)
	if err == nil || err.Error() != "not found" {
		t.Errorf("GetByID() error = %v, want %v", err, "not found")
	}

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			_, _ = storeStorage.GetByID(1)
			_, _ = storeStorage.GetByID(2)
			_, _ = storeStorage.GetByID(3)
		}()
	}
	wg.Wait()
}

func TestStoreStorage_GetInventoryByStatus(t *testing.T) {
	defer func() {
		_ = os.Remove(storeFile)
	}()

	testData := []*model.Store{
		{Status: "available"},
		{Status: "sold"},
		{Status: "available"},
		{Status: "pending"},
		{Status: "available"},
		{Status: "sold"},
		{Status: "sold"},
	}

	storeStorage := NewStoreStorage()
	storeStorage.data = testData

	inventory := storeStorage.GetInventoryByStatus()

	assert.Equal(t, 3, inventory["available"])
	assert.Equal(t, 3, inventory["sold"])
	assert.Equal(t, 1, inventory["pending"])
}
