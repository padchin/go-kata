package repository

import (
	"fmt"
	"gitlab.com/padchin/go-kata/module4/swagger_task/internal/model"
	f "gitlab.com/padchin/go-kata/module4/swagger_task/pkg/repository"
	"sync"
)

const petFile = "pet_data.json"

type PetStorager interface {
	Create(pet model.Pet) model.Pet
	Update(pet model.Pet) (model.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (model.Pet, error)
	GetList() []*model.Pet
	GetByStatus(status string) ([]*model.Pet, error)
}

type PetStorage struct {
	data               []*model.Pet
	primaryKeyIDx      map[int64]*model.Pet
	autoIncrementCount int64
	sync.RWMutex
}

func NewPetStorage() *PetStorage {
	data := make([]*model.Pet, 0)
	_ = f.LoadJSON(&data, petFile)

	primaryKeyIDx := make(map[int64]*model.Pet)
	for i, v := range data {
		primaryKeyIDx[int64(i)] = v
	}

	var autoIncrementCount int64
	if len(data) > 0 {
		autoIncrementCount = data[len(data)-1].ID + 1
	}

	return &PetStorage{
		autoIncrementCount: autoIncrementCount,
		data:               data,
		primaryKeyIDx:      primaryKeyIDx,
	}
}

func (p *PetStorage) GetByStatus(status string) ([]*model.Pet, error) {
	p.RLock()
	defer p.RUnlock()

	var pets []*model.Pet

	for _, v := range p.data {
		if v.Status == status {
			pets = append(pets, v)
		}
	}

	if len(pets) == 0 {
		return pets, fmt.Errorf("not found")
	}

	return pets, nil
}

func (p *PetStorage) Create(pet model.Pet) model.Pet {
	p.Lock()
	defer p.Unlock()

	_ = f.LoadJSON(&p.data, petFile)

	for _, v := range p.data {
		p.primaryKeyIDx[v.ID] = v
	}

	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	_ = f.DumpJSON(&p.data, petFile)

	return pet
}

func (p *PetStorage) Update(pet model.Pet) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.primaryKeyIDx[pet.ID]; !ok {
		return model.Pet{}, fmt.Errorf("pet not found")
	}

	if pet.ID != p.primaryKeyIDx[pet.ID].ID {
		return model.Pet{}, fmt.Errorf("new petID is not equal to old petID")
	}

	_pet := &model.Pet{
		ID:        pet.ID,
		Category:  pet.Category,
		Name:      pet.Name,
		PhotoUrls: pet.PhotoUrls,
		Tags:      pet.Tags,
		Status:    pet.Status,
	}

	for i, v := range p.data {
		if v.ID == pet.ID {
			p.data[i] = _pet
		}
	}

	p.primaryKeyIDx[pet.ID] = _pet

	_ = f.DumpJSON(&p.data, petFile)

	return pet, nil
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.primaryKeyIDx[int64(petID)]; !ok {
		return fmt.Errorf("pet not found")
	}

	p.data = append(p.data[:petID], p.data[petID+1:]...)
	delete(p.primaryKeyIDx, int64(petID))

	_ = f.DumpJSON(&p.data, petFile)

	return nil
}

func (p *PetStorage) GetByID(petID int) (model.Pet, error) {
	p.RLock()
	defer p.RUnlock()

	if v, ok := p.primaryKeyIDx[int64(petID)]; ok {
		return *v, nil
	}

	return model.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetList() []*model.Pet {
	p.RLock()
	defer p.RUnlock()

	return p.data
}
