package repository

import (
	"encoding/json"
	"fmt"
	"os"
)

func DumpJSON(obj interface{}, file string) error {
	j, err := json.MarshalIndent(obj, "", "    ")
	if err != nil {
		return fmt.Errorf("error encoding data: %s", err.Error())
	}

	err = os.WriteFile(file, j, 0644) //nolint:gosec
	if err != nil {
		return fmt.Errorf("error writing data: %s", err.Error())
	}

	return nil
}

func LoadJSON(obj interface{}, file string) error {
	j, err := os.ReadFile(file)
	if err != nil {
		return fmt.Errorf("error loading data: %s", err.Error())
	}

	err = json.Unmarshal(j, obj)
	if err != nil {
		return fmt.Errorf("error decoding data: %s", err.Error())
	}

	return nil
}
