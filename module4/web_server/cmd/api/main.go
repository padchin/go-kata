package main

import (
	"github.com/spf13/viper"
	"gitlab.com/padchin/go-kata/module4/web_server/config"
	"gitlab.com/padchin/go-kata/module4/web_server/server"

	"log"
)

func main() {
	if err := config.Init(); err != nil {
		log.Fatalf("%s", err.Error())
	}

	app := server.NewApp()

	if err := app.Run(viper.GetString("port")); err != nil {
		log.Fatalf("%s", err.Error())
	}
}
