package server

import (
	"context"
	"gitlab.com/padchin/go-kata/module4/web_server/auth/repository/localstorage"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/padchin/go-kata/module4/web_server/auth"
)

type App struct {
	httpServer *http.Server
	repository auth.UserRepository
}

func NewApp() *App {
	return &App{repository: localstorage.NewUserLocalStorage()}
}

func (a *App) Run(port string) error {
	//gin.SetMode(gin.ReleaseMode)
	f, err := os.Create("server.log")
	if err != nil {
		panic("error creating log: " + err.Error())
	}

	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "Welcome, viewers!"})
	})

	router.GET("/users", func(c *gin.Context) {
		resp, err := a.repository.GetUsers()

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "Users not found"})
		}

		c.JSON(http.StatusOK, resp)
	})

	router.GET("/users/:id", func(c *gin.Context) {
		id := c.Param("id")

		user, err := a.repository.GetUser(id)

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})

			return
		}

		c.JSON(http.StatusOK, user)
	})

	router.POST("/upload", func(c *gin.Context) {
		file, err := c.FormFile("file")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		filename := filepath.Base(file.Filename)
		if err := c.SaveUploadedFile(file, "public/"+filename); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "File uploaded successfully"})
	})

	router.GET("/public/:filename", func(c *gin.Context) {
		filename := c.Param("filename")
		c.File("public/" + filename)
	})

	a.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		if err := a.httpServer.ListenAndServe(); err != nil {
			log.Fatalf("Failed to listen and serve: %+v", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	<-quit

	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	return a.httpServer.Shutdown(ctx)
}
