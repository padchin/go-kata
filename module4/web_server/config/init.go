package config

import (
	"github.com/spf13/viper"
)

func Init() error {
	viper.AddConfigPath("module4/web_server/config")
	viper.SetConfigName("config")

	return viper.ReadInConfig()
}
