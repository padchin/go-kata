package auth

import (
	"gitlab.com/padchin/go-kata/module4/web_server/auth/repository/localstorage"
)

type UserRepository interface {
	GetUser(id string) (*localstorage.User, error)
	GetUsers() (map[string]*localstorage.User, error)
}
