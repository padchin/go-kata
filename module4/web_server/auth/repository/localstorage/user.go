package localstorage

import (
	"fmt"
	"sync"
)

type User struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

var users = map[string]*User{
	"1": {ID: "1", Name: "R2D2", Email: "r2d2@example.com", Password: "password1"},
	"2": {ID: "2", Name: "C3PO", Email: "c3po@example.com", Password: "password2"},
	"3": {ID: "3", Name: "Luke", Email: "luke@example.com", Password: "password3"},
}

type UserLocalStorage struct {
	users map[string]*User
	mx    *sync.Mutex
}

func NewUserLocalStorage() *UserLocalStorage {
	return &UserLocalStorage{users: users, mx: new(sync.Mutex)}
}

func (s *UserLocalStorage) GetUser(id string) (*User, error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	for _, user := range s.users {
		if user.ID == id {
			return user, nil
		}
	}

	return nil, fmt.Errorf("user %s not found", id)
}

func (s *UserLocalStorage) GetUsers() (map[string]*User, error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	return s.users, nil
}
